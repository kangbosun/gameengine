
#ifndef _SKYBOX_FX_
#define _SKYBOX_FX_

#include "VertexInput.fxh"
#include "Camera.fxh"
#include "CubeMapping.fxh"

struct PixelInput 
{
	float4 position : SV_Position;
	float3 texcoord : TexCoord0;
};


float4x4 worldMatrix;


PixelInput VS(VertexInput input)
{
	PixelInput output;
	output.position = mul(float4(input.position, 1), worldMatrix);
	output.position = mul(output.position, camera.viewMatrix);
	output.position = mul(output.position, camera.projectionMatrix);
	
	output.position = output.position.xyww;
	
	output.texcoord = input.position.xyz;
	
	return output;
}


float4 PS(PixelInput input) : SV_Target
{
	float4 color = cubeMapping.Sample(input.texcoord);
	return color;
}


technique11 tech
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}

#endif 