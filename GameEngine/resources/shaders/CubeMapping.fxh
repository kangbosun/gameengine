

#ifndef _CUBEMAPPING_FXH_
#define _CUBEMAPPING_FXH_

TextureCube cubeMap;
SamplerState cubeMapSampler
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};


interface ICubeMapping
{
	float4 Sample(float3 vec);
};


class CubeMappingEnabled : ICubeMapping
{
	float4 Sample(float3 vec)
	{
		return cubeMap.Sample(cubeMapSampler, vec);
	}
};

class CubeMappingDisabled : ICubeMapping
{
	float4 Sample(float3 vec)
	{
		return float4(0, 0, 0, 0);
	}
};

CubeMappingEnabled cubeMapEnable;
CubeMappingDisabled cubeMapDisable;

ICubeMapping cubeMapping = cubeMapDisable;

#endif 