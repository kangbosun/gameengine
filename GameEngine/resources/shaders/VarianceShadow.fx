
#include "Skinning.fxh"
#include "VertexInput.fxh"
#include "Camera.fxh"

matrix worldMatrix;

struct PixelInput
{
	float4 position : SV_Position;
	float2 texcoord : TEXCOORD0;
};


PixelInput VS(VertexInput input)
{
	PixelInput output;
	
	float4x4 skinnedMatrix = skinning.GetMatrix(input.boneIndices, input.weights);
	float4 pos = mul(float4(input.position,1), skinnedMatrix);
	
	output.position = mul(pos, worldMatrix);
	output.position = mul(output.position, camera.viewMatrix);
	output.position = mul(output.position, camera.projectionMatrix);
	output.texcoord = input.texcoord;
	return output;
}



float2 PS(PixelInput input) : SV_Target
{
	float depth = input.position.z / input.position.w;
	
	float2 moment;
	moment.x = depth;
	moment.y = depth* depth;
	float dx = ddx(depth);
	float dy = ddy(depth);
	moment.y += 0.25 * (dx*dx + dy*dy);
	
	return moment;
}

technique11 ShadowMap
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}




