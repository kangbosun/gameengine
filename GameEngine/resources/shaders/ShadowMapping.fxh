
#ifndef _SHADOWMAPPING_FXH_
#define _SHADOWMAPPING_FXH_


Texture2D shadowMap;
float4x4 shadowTransform;

SamplerState shadowMapSamplerState
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Border;
	AddressV = Border;
	BorderColor = float4(1, 1, 1, 1);
};

interface IShadowMapping
{
	float GetShadowFactor(float4 shadowPos);
	float4 GetVSShadowPos(float4 worldPos);
};

class CShadowMappingDisabled : IShadowMapping
{
	float GetShadowFactor(float4 shadowPos)
	{
		return 1;
	}
	float4 GetVSShadowPos(float4 worldPos)
	{
		return float4(0, 0, 0, 0);
	}
};

float gMinVariance = 0.000002f;

class CShadowMappingEnabled : IShadowMapping
{
	float GetShadowFactor(float4 shadowPos)
	{
		float3 ss = shadowPos.xyz / shadowPos.w;
		float2 texcoord = ss.xy;
		float dist = min(1, ss.z);

		float2 moment = shadowMap.Sample(shadowMapSamplerState, texcoord).rg;
		
		// Calculate ChebyshevUpperBound
		float p = (float)(dist <= moment.x);
				
		float variance = moment.y - (moment.x * moment.x);
		variance = max(variance, gMinVariance);
		
		float d = dist - moment.x;
		float pMax = variance / (variance + d * d);
		return smoothstep(0.4, 1, max(p, pMax));
	}
	float4 GetVSShadowPos(float4 worldPos)
	{
		return mul(worldPos, shadowTransform);
	}
};



CShadowMappingDisabled shadowMapDisable;
CShadowMappingEnabled shadowMapEnable;
IShadowMapping shadowMapping = shadowMapDisable;

#endif 