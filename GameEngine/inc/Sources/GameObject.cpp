#include "enginepch.h"
#include "GameObject.h"
#include "Renderer.h"
#include "Material.h"
#include "Mesh.h"
#include "Transform.h"
#include "Shader.h"
#include "Graphic.h"
#include "FbxLoader.h"

namespace GameEngine
{
	using namespace std;

	unordered_multimap<string, weak_ptr<GameObject>> GameObject::allGameObjects;
	shared_ptr<GameObject> GameObject::Root = GameObject::Instantiate("RootGameObject");


	GameObject::GameObject()
	{
		components.reserve(5);
	}
	
	GameObject::GameObject(const GameObject& gameObject) :
		transform(gameObject.transform), name(gameObject.name), active(gameObject.active)
	{
		components.reserve(5);
		
	}

	shared_ptr<Renderer> GameObject::GetRenderer()
	{
		if(_renderer.expired())
			_renderer = GetComponent<Renderer>();
		return _renderer.lock();
	}

	shared_ptr<GameObject> GameObject::Instantiate(const string& name) 
	{
		auto go = shared_ptr<GameObject>(new GameObject());
		go->name = name;
		go->transform.gameObject = go;
		return go;
	}

	shared_ptr<GameObject> GameObject::Instantiate(const shared_ptr<GameObject>& go)
	{
		auto clone = shared_ptr<GameObject>(new GameObject(*go));
		clone->transform.gameObject = clone;
		for(int i = 0; i < go->components.size(); ++i) 
			clone->AddComponent(go->components[i]->CloneShared());
		
		for(auto ci = go->children.begin(); ci != go->children.cend(); ++ci) {
			auto child = *ci;
			if(child) 
				Instantiate(child)->SetParent(clone);
		}
		return clone;
	}

	void GameObject::Destroy(shared_ptr<GameObject>& object)
	{
		if(object) {	
			for(auto child : object->children) {
				if(child)
					Destroy(child);
			}

			object->children.clear();
			object->components.clear();
			object->destroyed = true;
		}
	}

	void GameObject::Register(const shared_ptr<GameObject>& go)
	{
		if(go && !go->isRegistered) {
			allGameObjects.insert({ go->name, go });
			go->isRegistered = true;
			if(go->parent == nullptr) {
				go->SetParent(GameObject::Root);
			}

			for(int i = 0; i < go->components.size(); ++i) {
				Component::Register(go->components[i]);
			}

			for(auto child : go->children) {
				if(child)
					Register(child);
			}
		}
	}

	shared_ptr<GameObject> GameObject::FindGameObject(const std::string & name)
	{
		shared_ptr<GameObject> ret = nullptr;
		auto pair = allGameObjects.equal_range(name);
		if(pair.first != allGameObjects.cend())
			ret = pair.first->second.lock();
		return ret;
	}

	void GameObject::FindGameObjects(const std::string& name, std::vector<shared_ptr<GameObject>>& result)
	{
		auto pair = allGameObjects.equal_range(name);
		for(auto i = pair.first; i != pair.second; ++i) {
			result.push_back(i->second.lock());
		}
	}

	void GameObject::SetParent(const std::shared_ptr<GameObject>& go)
	{
		if(parent) {
			weak_ptr<GameObject> temp = shared_from_this();
			parent->children.remove_if(
				[temp](const weak_ptr<GameObject>& wp) {
				return wp.lock() == temp.lock();
			});
		}
		if(go == Root)
			parent = nullptr;
		else
		parent = go;
		go->children.push_back(shared_from_this());
	}

	shared_ptr<GameObject> GameObject::FindGameObjectInChildren(const std::string & name)
	{
		shared_ptr<GameObject> result = nullptr;
		if(this->name == name)
			result = shared_from_this();
		else {
			for(auto i = children.begin(); i != children.end(); ++i) {
				auto child = *i;
				if(!child->destroyed) {
					result = child->FindGameObjectInChildren(name);
					if(result) break;
				}
			}
		}
		return result;
	}

	void GameObject::FindGameObjectsInChildren(const std::string & name, std::vector<shared_ptr<GameObject>>& result)
	{
		if(this->name == name)
			result.push_back(shared_from_this());
		for(auto i = children.begin(); i != children.end(); ++i) {
			auto child = *i;
			if(!child->destroyed) {
				child->FindGameObjectsInChildren(name, result);
			}
		}
	}

	void GameObject::UpdateHierarchy()
	{
		transform.Update();
		for(auto it = children.begin(); it != children.cend(); ) {
			auto& child = *it;
			if(child->destroyed == true) {
				it = children.erase(it);		
			}
			else {
				child->UpdateHierarchy();
				++it;
			}
		}
	}

	void GameObject::SetActive(bool b, bool recursively)
	{
		active = b;
		if(recursively) {
			for(auto iter = children.begin(); iter != children.end();++iter) {
				(*iter)->SetActive(b, recursively);
			}
		}
	}

	shared_ptr<GameObject> GameObject::CloneShared()
	{
		return Instantiate(shared_from_this());
	}

	std::shared_ptr<Component> GameObject::AddComponent(const std::shared_ptr<Component>& _com)
	{
		if(_com) {
			components.push_back(_com);
			_com->gameObject = shared_from_this();
			if(isRegistered)
				Component::Register(_com);
		}
		return _com;
	}

	shared_ptr<Component> GameObject::GetComponent(const string& name)
	{
		for(auto com : components) {
			if(name.compare(com->ToString()) == 0)
				return com;
		}
		return shared_ptr<Component>();
	}

	void GameObject::RemoveComponent(const std::string & name)
	{
		for(auto& com : components) {
			if(com->ToString() == name) {
				swap(com, components.back());
				components.pop_back();
				return;
			}
		}
	}
}
