
#include "enginepch.h"

#include "Skybox.h"
#include "Resource.h"
#include "Shader.h"
#include "Texture2D.h"
#include "Camera.h"
#include "Mesh.h"
#include "GameObject.h"
#include "Renderer.h"

namespace GameEngine
{
	Skybox::Skybox()
	{
		skyShader = Resource::shaders.Find("Skybox");
		mesh = Resource::models.Find("sphere")->GetComponent<MeshRenderer>()->GetMesh();
	}

	Skybox::Skybox(const Skybox & rhs)
	{
		skyShader = rhs.skyShader;
		mesh = rhs.mesh;
		skyMap = rhs.skyMap;
		if(!skyShader)
			skyShader = Resource::shaders.Find("Skybox");
		if(!mesh)
			mesh = Resource::models.Find("sphere")->GetComponent<MeshRenderer>()->GetMesh();
	}



	void Skybox::Draw(const CameraData & cam)
	{
		if(!skyShader)
			return;
		if(!skyMap)
			return;

		auto graphic = Graphic::Get();
		graphic->SetCullMode(D3D11_CULL_FRONT);
		graphic->SetDSComparison(D3D11_COMPARISON_LESS_EQUAL);

		skyShader->SetCamera(&cam);

		Matrix mat;
		Vector3 scale = Vector3(size, size, size);
		Matrix::CreateTransform(mat, cam.position, Quaternion::Identity, scale);
		skyShader->SetWorldMatrix(&mat);
		skyShader->SetCubeMap(skyMap.get());

		graphic->SetMeshBuffer(mesh);
		skyShader->DrawIndices(mesh->nIndices(), 0);
		graphic->SetDSComparison(D3D11_COMPARISON_LESS);
		graphic->SetCullMode(D3D11_CULL_BACK);
	}
}
