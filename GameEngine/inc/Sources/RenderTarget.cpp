#include "enginepch.h"
#include "Texture2D.h"
#include "Graphic.h"
#include "DebugClass.h"

namespace GameEngine
{
	void RenderTarget::Initialize(int width, int height, TextureFormat format)
	{
		this->format = format;

		this->width = width;
		this->height = height;
		isValid = false;
		srv.Release();
		renderTargetView.Release();

		auto graphic = Graphic::Get();

		CreateTexture2D(width, height, format, USAGE_DEFAULT, BindFlag(BIND_RENDER_TARGET | BIND_SHADER_RESOURCE));

		HRESULT hr = graphic->GetDevice()->CreateRenderTargetView(texture2D, 0, &renderTargetView.p);
		if(FAILED(hr)) {
			Debug::Log(ToString() + " - failed to create RTV");
			isValid = false;
			return;
		}
		isValid = true;
	}

	void RenderTarget::Initialize(int width, int height, TextureFormat format, const CComPtr<ID3D11Texture2D>& buffer)
	{
		this->format = format;
		this->width = width;
		this->height = height;
		isValid = false;
		srv.Release();
		renderTargetView.Release();

		auto graphic = Graphic::Get();

		HRESULT hr = graphic->GetDevice()->CreateRenderTargetView(buffer, 0, &renderTargetView.p);
		if(FAILED(hr)) {
			Debug::Log(ToString() + " - failed to create RTV");
			isValid = false;
			return;
		}
		isValid = true;
	}
}