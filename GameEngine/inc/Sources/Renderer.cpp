#include "enginepch.h"
#include "Renderer.h"
#include "Resource.h"
#include "GameObject.h"
#include "Light.h"
#include "Mesh.h"
#include "Shader.h"
#include "Material.h"
#include "Graphic.h"
#include "Transform.h"
#include "GlobalSetting.h"
#include "Camera.h"
#include "Texture2D.h"


namespace GameEngine
{
	
	using namespace std;
	// MeshRenderer

	list<weak_ptr<MeshRenderer>> MeshRenderer::allMeshRenderers;

	MeshRenderer::MeshRenderer()
	{
		type = Renderer::eMesh;
	}

	void MeshRenderer::Start()
	{
		if(rootBoneName != "") {
			auto root = GetGameObject();
			while(root->GetParent() && root->GetParent()->name != "RootGameObject")
				root = root->GetParent();
			auto rootBone = root->FindGameObjectInChildren(rootBoneName);
			SetBone(rootBone);
		}
		allMeshRenderers.push_back(GetGameObject()->GetComponent<MeshRenderer>());
	}

	void MeshRenderer::SetBone(const shared_ptr<GameObject>& rootBone)
	{
		if(rootBone) {
			bones.push_back(rootBone);
			auto& children = rootBone->GetChildren();
			for(auto i = children.begin(); i != children.cend(); ++i)
				SetBone(*i);
		}
	}

	void MeshRenderer::Update()
	{
		int boneSize = (int)bones.size();
		int bindposeInvSize = (int)mesh->bindPoseInverse.size();
		if(boneSize > 0 && boneSize == bindposeInvSize) {
			matrices.clear();
			matrices.reserve(bones.size());
			Matrix offsetInv;
			auto parent = bones[0]->GetParent();
			if(parent)
				parent->transform.WorldMatrix().Inverse(offsetInv);
			else offsetInv = Matrix::Identity;

			for(int bi = 0; bi < boneSize; ++bi) {
				Matrix& bindposeInv = mesh->bindPoseInverse[bi];
				Matrix& world = bones[bi]->transform.WorldMatrix();
				Matrix& final = bindposeInv * world * offsetInv;
				matrices.push_back(std::move(final));
			}
		}
	}

	void MeshRenderer::Render(const CameraData& cam, const shared_ptr<Shader>& forcedShader)
	{
		auto graphic = Graphic::Get();
		if(!graphic)
			return;	
		if(!mesh || !mesh->IsValid()) return;

		bool renderShadow = false;
		bool useSkinning = false;

		auto& lights = Light::allLights;
		//string name = GetGameObject()->name;

		int offset = 0;
		graphic->SetMeshBuffer(mesh);

		for(int i = 0; i < materials.size(); ++i) {
			auto& material = materials[i];
			if(!material) continue;

			auto& shader = forcedShader ? forcedShader : material->shader;
			if(!shader) continue;

			shader->SetGlobalSetting(&GlobalSetting::graphicSetting);
			shader->SetCamera(&cam);
			auto envMap = Camera::main->GetSkyMap();
			shader->SetCubeMap(envMap.get());

			shader->SetWorldMatrix(&GetTransform()->WorldMatrix());

			if(matrices.size() > 0)
				shader->SetBoneMatrices(&(*matrices.begin()), (uint32_t)matrices.size());
			else
				shader->SetBoneMatrices(0, 0);

			auto& light = lights.begin()->lock();
			shader->SetLight(&light->GetLightData());

			shader->SetMaterial(&material->data);
			shader->SetDiffuseMap(material->diffuseMap.get());
			shader->SetNormalMap(material->normalMap.get());

			if(light->renderShadow && receiveShadow) {
				auto& pos1 = GetTransform()->GetPosition();
				auto& pos2 = cam.position;
				float dist = (pos1 - pos2).Length();
				if(dist <= light->shadowDistance)
					shader->SetShadowMap(light->GetShadowMap()->GetTexture().get(), &light->GetShadowTransform());
				else
					shader->SetShadowMap(0, 0);
			}
			else
				shader->SetShadowMap(0, 0);

			UINT subMeshCount = mesh->GetSubMeshCount(i);
			shader->DrawIndices(subMeshCount, offset);
			offset += subMeshCount;
		}
	}

	void MeshRenderer::RenderAll(const CameraData& cam)
	{
		for(auto iter = allMeshRenderers.begin(); iter != allMeshRenderers.end();) {
			if(iter->expired())
				iter = allMeshRenderers.erase(iter);
			else {
				auto& renderer = iter->lock();
				if(renderer->GetGameObject()->GetActive() && renderer->enabled)
				renderer->Render(cam, nullptr);
				iter++;
			}
		}
	}

	void MeshRenderer::RenderAllToShadowMap(Light& light)
	{
		for(auto iter = allMeshRenderers.begin(); iter != allMeshRenderers.end();) {
			if(iter->expired())
				iter = allMeshRenderers.erase(iter);
			else {
				auto _renderer = iter->lock();
				if(_renderer->castShadow && _renderer->enabled && _renderer->GetGameObject()->GetActive()) {
					auto& pos1 = _renderer->GetTransform()->GetPosition();
					auto& pos2 = Camera::main->GetTransform()->GetPosition();
					auto& forward = Camera::main->GetTransform()->forward();

					float dist = (pos1 - pos2).Length();					
					if(dist > 0 && dist < 10)
						_renderer->Render(light.GetCameraData(), light.GetShadowShader());
				}
				++iter;
			}
		}
	}

	//////////////////////////////////////////////////////////

	std::list<std::weak_ptr<UIRenderer>> UIRenderer::allUIRenderers;

	UIRenderer::UIRenderer()
	{
		material.shader = Resource::shaders.Find("Texture");
		material.data.color = Color::White;
		type = RendererType::eUI;
	}

	void UIRenderer::Start()
	{
		allUIRenderers.push_back(GetGameObject()->GetComponent<UIRenderer>());
	}

	void UIRenderer::Render(const CameraData& cam, const std::shared_ptr<Shader>& _shader)
	{
		string name = GetGameObject()->name;
		auto graphic = Graphic::Get();
		
		if(!graphic) return;
		
		if(!mesh.get() || mesh->IsValid() == false)
			return;
		auto& uishader = _shader ? _shader : material.shader;
		
		if(!uishader) return;

		Vector3 s, t;
		Quaternion q;

		Matrix world = GetTransform()->WorldMatrix();
		world.Decompose(t, q, s);

		auto size = GetTransform()->GetSize();

		auto scaleFactor = Graphic::Get()->GetUIScale();

		t.x *= scaleFactor.x; t.y *= scaleFactor.y;
		s.x *= scaleFactor.x; s.y *= scaleFactor.y;

		Matrix::CreateTransform(world, t, q, s);

		uishader->SetWorldMatrix(&world);
		uishader->SetMaterial(&material.data);
		uishader->SetCamera(&cam);
		uishader->SetDiffuseMap(material.diffuseMap.get());

		//Graphic::Instance()->SetAlphaBlend(useAlphaBlending);
		graphic->SetMeshBuffer(mesh);
		uishader->DrawIndices(mesh->nIndices());
	}

	void UIRenderer::RenderAll(const CameraData& cam, const std::shared_ptr<Shader>& shader)
	{
		for(auto iter = allUIRenderers.begin(); iter != allUIRenderers.end();) {
			if(iter->expired())
				iter = allUIRenderers.erase(iter);
			else {
				auto _renderer = iter->lock();
				//int d = _renderer->transform()->worldPosition->z;
				//auto& name = _renderer->GetGameObject()->name;
				iter->lock()->Render(cam, shader);
				iter++;
			}
		}
		allUIRenderers.sort();
	}

	bool operator <(const std::weak_ptr<UIRenderer>& lhs, const std::weak_ptr<UIRenderer>& rhs)
	{
		float a = lhs.lock()->GetTransform()->worldPosition().z;
		float b = rhs.lock()->GetTransform()->worldPosition().z;

		return a > b;
	}



	///////////////////////////////////////
}