#include "enginepch.h"
#include "Texture2D.h"
#include "Graphic.h"

namespace GameEngine
{
	void DepthStencil::Initialize(int width, int height)
	{
		this->format = D24_UNORM_S8_UINT;
		this->width = width;
		this->height = height;
		isValid = false;

		depthStencilView.Release();
		srv.Release();

		CreateTexture2D(width, height, format, USAGE_DEFAULT, BIND_DEPTH_STENCIL);

		auto graphic = Graphic::Get();

		auto hr = graphic->GetDevice()->CreateDepthStencilView(texture2D, 0, &depthStencilView.p);
		if(FAILED(hr)) {
			isValid = false;
			return;
		}
		isValid = true;
	}
}