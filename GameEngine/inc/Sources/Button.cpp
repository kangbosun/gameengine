#include "enginepch.h"
#include "Transform.h"
#include "UI.h"
#include "Resource.h"
#include "GameObject.h"
#include "Texture2D.h"

namespace GameEngine
{
	void Button::Start()
	{
		auto go = GetGameObject();
		if(go) {
			_text = go->GetComponentInChildren<Text>();
			_image = go->GetComponentInChildren<Image>();
		}
	}

	std::shared_ptr<Button> Button::CreateButton(const Vector3& pos, const Vector2& size, const std::wstring& str)
	{
		using namespace std;
		auto g = GameObject::Instantiate("Button");

		auto text = Text::CreateText(Vector3(0, 0, -0.1f), size, str);
		text->SetColor(Color::Black);
		text->SetAlign(Align(eCenter | eCenter));

		auto img = g->AddComponent<Image>();
		img->SetSize(size.x, size.y);
		img->SetTexture(Resource::textures.Find("white"));

		text->GetGameObject()->SetParent(g);
		auto& button = g->AddComponent<Button>();
		button->Start();
		button->SetSize(size.x, size.y);
		button->GetTransform()->SetPosition(pos);

		g->AddComponent<UISelector>();

		return button;
	}

	void Button::SetText(const std::wstring& str)
	{
		if(_text) {
			_text->SetText(str);
		}
	}

	void Button::SetSize(float width, float height)
	{
		if(_text) {
			_text->SetFontSize((int)(height * 0.5f));
			_text->GetTransform()->SetSize(width, height);
			_image->SetSize(width, height);
		}
	}
}