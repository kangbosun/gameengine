#pragma once
#pragma warning(push)
#pragma warning(disable:4251)

namespace GameEngine
{
	class GameTime;
	class Graphic;
	class Scene;
	struct Audio;

	class GAMEENGINE_API GameWindow
	{
	private:
		static LRESULT CALLBACK static_WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	protected:
		HWND hWnd = 0;
		int width = 0;
		int height = 0;
		int positionX = 0;
		int positionY = 0;
		int id = -1;
		
		bool running = false;

		int updatePerSecond;
		int framePerSecond;

		float updateTime;
		float frameTime;

		float elaspedTime;

		std::shared_ptr<Graphic> graphic;
		std::shared_ptr<Audio> audio;

		std::shared_ptr<Scene> currScene;

	private:
		bool Initialize(LPCWSTR title, int width, int height, int _framePerSecond = 60);
		void Update();
		void Render();

	public:
		static std::unordered_map<int, std::shared_ptr<GameWindow>> allWindows;
		static std::shared_ptr<GameWindow> Get(int _id = 0);

		int Run(int fps = 60);

		std::shared_ptr<Graphic> GetGraphic() { return graphic; }
		bool Running() { return running; }	
		LRESULT WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		
		void LoadScene(const std::shared_ptr<Scene>& scene);
		void UnloadScene();

		Rect GetRect() { return{ width, height, positionX, positionY }; }
		HWND GetHandle() { return hWnd; }
		int GetID() { return id; }

		void Destroy();

		static std::shared_ptr<GameWindow> Create(const std::wstring& title, int width, int height, int framePerSec = 60);
	};
}
#pragma warning(pop)