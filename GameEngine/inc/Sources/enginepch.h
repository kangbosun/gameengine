#pragma once

#include <unordered_map>
#include <map>
#include <vector>
#include <list>
#include <atlmem.h>
#include <memory>
#include <string>
#include <stack>
#include <iostream>
#include <chrono>
#include <algorithm>
#include <array>
#include <functional>
#include <fstream>
#include <sstream>
#include <tchar.h>
#include <set>
#include <queue>

#ifdef _WIN64
#include <Windows.h>

#include <d3d11.h>
#include <DirectXCollision.h>
#include <DirectXMath.h>

#include <FX11\d3dx11effect.h>
#include <d3dcompiler.h>

#pragma comment(lib, "Effects11.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")

#endif


#define FBXSDK_SHARED
#include <fbxsdk.h>
#pragma comment(lib, "libfbxsdk.lib")

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_MODULE_H
#pragma comment(lib, "freetype26.lib")

//engine
#include "EngineDefines.h"
#include "MathLib.h"
#include "Object.h"
#include "Component.h"
#include "Transform.h"
