
#include "enginepch.h"
#include "Audio.h"

#define AL_ALEXT_PROTOTYPES
#include <AL\al.h>
#include <AL\alc.h>
#include <AL\alext.h>
#include <AL\efx-presets.h>

#pragma comment(lib, "OpenAL32.lib")

namespace GameEngine
{
	AudioEffectReverb::AudioEffectReverb()
	{
		alGenEffects(1, &effectID);
		alGenAuxiliaryEffectSlots(1, &slotID);
		EFXEAXREVERBPROPERTIES prop = EFX_REVERB_PRESET_BATHROOM;
		auto enableEAXReverb = alGetEnumValue("AL_EFFECT_EAXREVERB");
		if(enableEAXReverb) {
			//using EAX
			alEffecti(effectID, AL_EFFECT_TYPE, AL_EFFECT_EAXREVERB);
			alEffectf(effectID, AL_EAXREVERB_DENSITY, prop.flDensity);
			alEffectf(effectID, AL_EAXREVERB_DIFFUSION, prop.flDiffusion);
			alEffectf(effectID, AL_EAXREVERB_GAIN, prop.flGain);
			alEffectf(effectID, AL_EAXREVERB_GAINHF, prop.flGainHF);
			alEffectf(effectID, AL_EAXREVERB_GAINLF, prop.flGainLF);
			alEffectf(effectID, AL_EAXREVERB_DECAY_TIME, prop.flDecayTime);
			alEffectf(effectID, AL_EAXREVERB_DECAY_HFRATIO, prop.flDecayHFRatio);
			alEffectf(effectID, AL_EAXREVERB_DECAY_LFRATIO, prop.flDecayLFRatio);
			alEffectf(effectID, AL_EAXREVERB_REFLECTIONS_GAIN, prop.flReflectionsGain);
			alEffectf(effectID, AL_EAXREVERB_REFLECTIONS_DELAY, prop.flReflectionsDelay);
			alEffectfv(effectID, AL_EAXREVERB_REFLECTIONS_PAN, prop.flReflectionsPan);
			alEffectf(effectID, AL_EAXREVERB_LATE_REVERB_GAIN, prop.flLateReverbGain);
			alEffectf(effectID, AL_EAXREVERB_LATE_REVERB_DELAY, prop.flLateReverbDelay);
			alEffectfv(effectID, AL_EAXREVERB_LATE_REVERB_PAN, prop.flLateReverbPan);
			alEffectf(effectID, AL_EAXREVERB_ECHO_TIME, prop.flEchoTime);
			alEffectf(effectID, AL_EAXREVERB_ECHO_DEPTH, prop.flEchoDepth);
			alEffectf(effectID, AL_EAXREVERB_MODULATION_TIME, prop.flModulationTime);
			alEffectf(effectID, AL_EAXREVERB_MODULATION_DEPTH, prop.flModulationDepth);
			alEffectf(effectID, AL_EAXREVERB_AIR_ABSORPTION_GAINHF, prop.flAirAbsorptionGainHF);
			alEffectf(effectID, AL_EAXREVERB_HFREFERENCE, prop.flHFReference);
			alEffectf(effectID, AL_EAXREVERB_LFREFERENCE, prop.flLFReference);
			alEffectf(effectID, AL_EAXREVERB_ROOM_ROLLOFF_FACTOR, prop.flRoomRolloffFactor);
			alEffecti(effectID, AL_EAXREVERB_DECAY_HFLIMIT, prop.iDecayHFLimit);
		}
		else {
			alEffecti(effectID, AL_EFFECT_TYPE, AL_EFFECT_REVERB);
			alEffectf(effectID, AL_REVERB_DENSITY, prop.flDensity);
			alEffectf(effectID, AL_REVERB_DIFFUSION, prop.flDiffusion);
			alEffectf(effectID, AL_REVERB_GAIN, prop.flGain);
			alEffectf(effectID, AL_REVERB_GAINHF, prop.flGainHF);
			alEffectf(effectID, AL_REVERB_DECAY_TIME, prop.flDecayTime);
			alEffectf(effectID, AL_REVERB_DECAY_HFRATIO, prop.flDecayHFRatio);
			alEffectf(effectID, AL_REVERB_REFLECTIONS_GAIN, prop.flReflectionsGain);
			alEffectf(effectID, AL_REVERB_REFLECTIONS_DELAY, prop.flReflectionsDelay);
			alEffectf(effectID, AL_REVERB_LATE_REVERB_GAIN, prop.flLateReverbGain);
			alEffectf(effectID, AL_REVERB_LATE_REVERB_DELAY, prop.flLateReverbDelay);
			alEffectf(effectID, AL_REVERB_AIR_ABSORPTION_GAINHF, prop.flAirAbsorptionGainHF);
			alEffectf(effectID, AL_REVERB_ROOM_ROLLOFF_FACTOR, prop.flRoomRolloffFactor);
			alEffecti(effectID, AL_REVERB_DECAY_HFLIMIT, prop.iDecayHFLimit);
		}
		alAuxiliaryEffectSloti(slotID, AL_EFFECTSLOT_EFFECT, effectID);
		effectName = "reverb";
		valid = true;
	}
}