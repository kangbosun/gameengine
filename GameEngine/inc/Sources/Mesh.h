#pragma once

#pragma warning(push)
#pragma warning(disable:4251)

namespace GameEngine
{

	struct Vertex;

	class GAMEENGINE_API Mesh final
	{
		friend class Graphic;
	private:
		CComPtr<ID3D11Buffer> vertexBuffer = nullptr;
		CComPtr<ID3D11Buffer> indexBuffer = nullptr;
		UINT nVertex = 0;
		UINT nIndex = 0;
		UINT stride = 0;
		UINT offset = 0;
		D3D11_PRIMITIVE_TOPOLOGY primitiveType;
		bool valid = false;
		
		std::vector<UINT> vertCountOfSubMesh;
		Mesh() {}
	public:
		std::vector<Matrix> bindPoseInverse;

	public:	
		UINT nIndices() { return nIndex; }
		bool IsValid() { return valid; }
		UINT GetSubMeshCount(UINT index);
	};
}

#pragma warning(pop)