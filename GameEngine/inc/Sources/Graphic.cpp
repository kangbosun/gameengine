#include "enginepch.h"
#include "DirectXAbstract.h"
#include "GameWindow.h"
#include "Graphic.h"
#include "DebugClass.h"
#include "GlobalSetting.h"
#include "Texture2D.h"
#include "Mesh.h"

namespace GameEngine
{
	using namespace std;

	unordered_map<int, shared_ptr<Graphic>> Graphic::allGraphics;

	shared_ptr<Graphic> Graphic::Get(int _id)
	{
		auto f = allGraphics.find(_id);
		if(f != allGraphics.cend())
			return f->second;
		return nullptr;
	}

	void Graphic::Initialize(int width, int height, GameWindow* window)
	{
		if(!window)
			return;
		this->window = window;

		allGraphics[window->GetID()] = shared_from_this();

		sampleDesc.Count = 1;
		sampleDesc.Quality = 0;
		fillMode = D3D11_FILL_MODE::D3D11_FILL_SOLID;

		UINT creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

#if defined(_DEBUG)
		creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

		HRESULT result;

		IDXGIFactory* factory;
		result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
		if(FAILED(result)) Debug::Log("failed to create DXGIFactory");

		IDXGIAdapter* adapter;
		result = factory->EnumAdapters(0, &adapter);
		if(FAILED(result)) Debug::Log("failed to factory->EnumAdapters");

		IDXGIOutput* adapterOut;
		result = adapter->EnumOutputs(0, &adapterOut);
		if(FAILED(result)) Debug::Log("failed to adapter->EnumOutputs");

		unsigned int numModes;
		result = adapterOut->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);

		std::vector<DXGI_MODE_DESC> displayModes(numModes);		
		result = adapterOut->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, &displayModes[0]);
		if(FAILED(result)) Debug::Log("failed to adapterOut->GetDisplayModeList");


		int numerator = displayModes[numModes - 1].RefreshRate.Numerator;
		int denominator = displayModes[numModes - 1].RefreshRate.Denominator;

		int maxWidth = displayModes[numModes - 1].Width;
		int maxHeight = displayModes[numModes - 1].Height;
		//int maxWidth = 1920;
		//int maxHeight = 1080;

		width = maxWidth;
		height = maxHeight;

		DXGI_ADAPTER_DESC adapterDesc;
		result = adapter->GetDesc(&adapterDesc);
		if(FAILED(result)) Debug::Log("failed to adapter->GetDesc");
		videoMemory = (int)(adapterDesc.DedicatedVideoMemory >> 20);

		videoCardDesc = adapterDesc.Description;

		videoCardDesc += (L"(" + to_wstring(videoMemory) + L"MB)");
		Debug::Log(videoCardDesc);

		adapterOut->Release();
		adapter->Release();
		factory->Release();

		DXGI_SWAP_CHAIN_DESC scd = { 0 };
		scd.BufferCount = 1;
		scd.BufferDesc.Width = width;
		scd.BufferDesc.Height = height;
		scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		scd.BufferDesc.RefreshRate.Numerator = numerator;
		scd.BufferDesc.RefreshRate.Denominator = denominator;
		scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scd.OutputWindow = window->GetHandle();
		scd.SampleDesc.Quality = sampleDesc.Quality;
		scd.SampleDesc.Count = sampleDesc.Count;
		scd.Windowed = true;
		scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		scd.BufferDesc.Scaling = DXGI_MODE_SCALING::DXGI_MODE_SCALING_UNSPECIFIED;
		scd.SwapEffect = DXGI_SWAP_EFFECT::DXGI_SWAP_EFFECT_DISCARD;

		D3D_FEATURE_LEVEL featureLevels[] = {
			D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_11_0
		};
		result = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE::D3D_DRIVER_TYPE_HARDWARE, 0, 0, featureLevels,
											   ARRAYSIZE(featureLevels), D3D11_SDK_VERSION, &scd, &swapChain, &device, NULL, &deviceContext);
		if(FAILED(result))
			Debug::Log("failed to create Device and Swapchain");

		renderTarget = std::make_shared<RenderTarget>();
		depthStencil = std::make_shared<DepthStencil>();

		Resize(width, height);

		D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
		ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

		depthStencilDesc.DepthEnable = true;
		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
		depthStencilDesc.StencilEnable = true;
		depthStencilDesc.StencilReadMask = 0xFF;
		depthStencilDesc.StencilWriteMask = 0xFF;
		depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		device->CreateDepthStencilState(&depthStencilDesc, &depthStencilState);

		deviceContext->OMSetDepthStencilState(depthStencilState, 1);

		rasterizerDesc.AntialiasedLineEnable = false;
		rasterizerDesc.CullMode = D3D11_CULL_MODE::D3D11_CULL_BACK;
		rasterizerDesc.DepthBias = 0;
		rasterizerDesc.DepthBiasClamp = 0.0f;
		rasterizerDesc.DepthClipEnable = true;
		rasterizerDesc.FillMode = D3D11_FILL_MODE::D3D11_FILL_SOLID;
		rasterizerDesc.FrontCounterClockwise = false;
		rasterizerDesc.MultisampleEnable = false;
		rasterizerDesc.ScissorEnable = false;
		rasterizerDesc.SlopeScaledDepthBias = 0.0f;

		result = device->CreateRasterizerState(&rasterizerDesc, &rasterizeState.p);
		if(FAILED(result)) Debug::Log("failed to create RasterizerState");

		deviceContext->RSSetState(rasterizeState);

		D3D11_BLEND_DESC bdesc = { 0 };
		bdesc.RenderTarget[0].BlendEnable = true;
		bdesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		bdesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		bdesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		bdesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		bdesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		bdesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		bdesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

		result = device->CreateBlendState(&bdesc, &blendState);
		if(FAILED(result)) Debug::Log("failed to create BlendState");

		bdesc.RenderTarget[0].BlendEnable = false;
		result = device->CreateBlendState(&bdesc, &disableBlendState);
		if(FAILED(result)) Debug::Log("failed to create BlendState");

		currDepthStencil = depthStencil;
		currRenderTarget = renderTarget;

		SetAlphaBlend(true);
		vSync = true;
	}

	void Graphic::SetCullMode(D3D11_CULL_MODE mode)
	{
		rasterizerDesc.CullMode = mode;
		rasterizeState.Release();
		device->CreateRasterizerState(&rasterizerDesc, &rasterizeState.p);
		deviceContext->RSSetState(rasterizeState);
	}

	void Graphic::SetFillMode(D3D11_FILL_MODE mode)
	{
		rasterizerDesc.FillMode = mode;
		rasterizeState.Release();
		device->CreateRasterizerState(&rasterizerDesc, &rasterizeState.p);
		deviceContext->RSSetState(rasterizeState);
	}

	void Graphic::SetDepthEnable(bool b)
	{
		D3D11_DEPTH_STENCIL_DESC desc;
		depthStencilState->GetDesc(&desc);
		
		if(b)
			desc.DepthEnable = true;
		else
			desc.DepthEnable = false;

		device->CreateDepthStencilState(&desc, &depthStencilState.p);
		deviceContext->OMSetDepthStencilState(depthStencilState.p, 1);
	}

	void Graphic::Resize(int width, int height)
	{
		TextureFormat format = R8G8B8A8_UNORM;
		swapChain->ResizeBuffers(1, width, height, (DXGI_FORMAT)format, 0);
		CComPtr<ID3D11Texture2D> backBufferPtr;
		swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBufferPtr.p);

		renderTarget->msaa = sampleDesc.Count;
		renderTarget->Initialize(width, height, format, backBufferPtr);
		depthStencil->msaa = sampleDesc.Count;
		depthStencil->Initialize(width, height);

		deviceContext->OMSetRenderTargets(1, &renderTarget->renderTargetView.p, depthStencil->depthStencilView);

		viewPort.Width = (float)width;
		viewPort.Height = (float)height;
		viewPort.MaxDepth = 1.0f;
		viewPort.MinDepth = 0.0f;
		viewPort.TopLeftX = 0.0f;
		viewPort.TopLeftY = 0.0f;

		deviceContext->RSSetViewports(1, &viewPort);

		resolution = Vector2(width, height);
		UIScale = Vector2(width / 1920.0f, height / 1080.0f);
		aspectRatio = (float)width / (float)height;
	}

	inline const CComPtr<ID3D11DepthStencilView>& Graphic::GetDepthStencilView() { return depthStencil->depthStencilView; }

	void Graphic::SwapBuffers()
	{
		swapChain->Present(vSync, 0);
	}

	void Graphic::ClearRenderTarget(const Color& color)
	{
		deviceContext->ClearRenderTargetView(currRenderTarget->renderTargetView, (float*)&color);
	}

	void Graphic::ClearDepthStencil()
	{
		deviceContext->ClearDepthStencilView(currDepthStencil->depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	}

	void Graphic::SetViewport(const Viewport& viewport)
	{
		deviceContext->RSSetViewports(1, &viewport);
	}

	int Graphic::VMemory()
	{
		return videoMemory;
	}

	wstring Graphic::VideoCardName()
	{
		return videoCardDesc;
	}

	void Graphic::SetAlphaBlend(bool b)
	{
		float blendFactor[4] = { 0.0f };
		if(b)
			deviceContext->OMSetBlendState(blendState.p, blendFactor, 0xffffffff);
		else
			deviceContext->OMSetBlendState(disableBlendState.p, blendFactor, 0xffffffff);
	}

	void Graphic::SetVsync(bool b)
	{
		if(b)
			vSync = 1;
		else
			vSync = 0;
	}

	bool Graphic::GetVsync()
	{
		return (vSync != 0);
	}

	void Graphic::SetDSComparison(D3D11_COMPARISON_FUNC comparison)
	{
		D3D11_DEPTH_STENCIL_DESC desc;
		depthStencilState->GetDesc(&desc);
		desc.DepthFunc = comparison;
		device->CreateDepthStencilState(&desc, &depthStencilState.p);
		deviceContext->OMSetDepthStencilState(depthStencilState.p, 1);
	}


	std::shared_ptr<Mesh> Graphic::CreateMesh(std::vector<Vertex>& vertices, std::vector<unsigned long>& indices, std::vector<UINT>* vertsOfSub, D3D11_PRIMITIVE_TOPOLOGY primitive) const
	{
		if(!device)
			return nullptr;

		auto mesh = shared_ptr<Mesh>(new Mesh());
		mesh->primitiveType = primitive;

		D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
		D3D11_SUBRESOURCE_DATA vertexData, indexData;
		HRESULT result;

		mesh->nVertex = (UINT)vertices.size();
		mesh->nIndex = (UINT)indices.size();

		if(vertsOfSub)
			mesh->vertCountOfSubMesh.swap(*vertsOfSub);
		else
			mesh->vertCountOfSubMesh.push_back(mesh->nVertex);

		mesh->stride = sizeof(Vertex);

		vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		vertexBufferDesc.ByteWidth = mesh->stride * mesh->nVertex;
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		vertexBufferDesc.CPUAccessFlags = 0;
		vertexBufferDesc.MiscFlags = 0;
		vertexBufferDesc.StructureByteStride = 0;

		vertexData.pSysMem = &vertices[0];
		vertexData.SysMemPitch = 0;
		vertexData.SysMemSlicePitch = 0;

		result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &mesh->vertexBuffer);
		if(FAILED(result)) {
			Debug::Log("Failed to create Vertex Buffer");
			return false;
		}

		indexBufferDesc.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
		indexBufferDesc.ByteWidth = sizeof(unsigned long) * mesh->nIndex;
		indexBufferDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;
		indexBufferDesc.StructureByteStride = 0;

		indexData.pSysMem = &indices[0];
		indexData.SysMemPitch = 0;
		indexData.SysMemSlicePitch = 0;

		result = device->CreateBuffer(&indexBufferDesc, &indexData, &mesh->indexBuffer);
		if(FAILED(result)) {
			Debug::Log("Failed to create to Index Buffer");
			return false;
		}
		mesh->valid = true;
		return mesh;
	}

	void Graphic::SetMeshBuffer(const std::shared_ptr<Mesh>& mesh) const
	{
		if(deviceContext && mesh && mesh->IsValid()) {
			auto& vertexBuffer = mesh->vertexBuffer;
			auto& indexBuffer = mesh->indexBuffer;
			deviceContext->IASetVertexBuffers(0, 1, (&vertexBuffer.p), &mesh->stride, &mesh->offset);
			deviceContext->IASetIndexBuffer(indexBuffer.p, DXGI_FORMAT_R32_UINT, 0);
			deviceContext->IASetPrimitiveTopology(mesh->primitiveType);
		}
	}

	void Graphic::SetBackBufferRenderTarget()
	{
		deviceContext->OMSetRenderTargets(1, &renderTarget->renderTargetView.p, depthStencil->depthStencilView);
		deviceContext->RSSetViewports(1, &viewPort);
		currDepthStencil = depthStencil;
		currRenderTarget = renderTarget;
	}

	void Graphic::SetRenderTarget(const std::shared_ptr<RenderTarget>& _renderTarget)
	{
		currRenderTarget = _renderTarget;
		deviceContext->OMSetRenderTargets(1, &_renderTarget->renderTargetView.p, 0);
	}

	void Graphic::SetRenderTarget(const std::shared_ptr<RenderTarget>& _renderTarget, const std::shared_ptr<DepthStencil>& _depthStencil)
	{
		currRenderTarget = _renderTarget;
		currDepthStencil = _depthStencil;
		deviceContext->OMSetRenderTargets(1, &_renderTarget->renderTargetView.p, _depthStencil->depthStencilView);
	}
}