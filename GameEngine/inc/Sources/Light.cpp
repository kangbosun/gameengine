#include "enginepch.h"
#include "Light.h"
#include "Transform.h"
#include "Graphic.h"
#include "Shader.h"
#include "Resource.h"
#include "GameObject.h"
#include "GlobalSetting.h"
#include "DebugClass.h"
#include "Renderer.h"
#include "Texture2D.h"

namespace GameEngine
{
	using namespace std;
	

	void ShadowMap::Bind(std::shared_ptr<Graphic>& graphicDevice)
	{
		graphicDevice->SetRenderTarget(renderTarget, depthStencil);
		graphicDevice->SetViewport(viewport);
		graphicDevice->SetDepthEnable(true);
		graphicDevice->ClearRenderTarget(Color::White);
		graphicDevice->ClearDepthStencil();
	}

	void ShadowMap::Resize(int mapSize)
	{
		if(renderTarget)
			renderTarget->Initialize(mapSize, mapSize, R32G32_FLOAT);
		if(depthStencil)
			depthStencil->Initialize(mapSize, mapSize);
	}

	void ShadowMap::Initialize(int mapSize)
	{
		size = mapSize;
		viewport.TopLeftX = 0.0f;
		viewport.TopLeftY = 0.0f;
		viewport.Width = (float)size;
		viewport.Height = (float)size;
		viewport.MaxDepth = 1.0f;
		viewport.MinDepth = 0.0f;

		renderTarget = make_shared<RenderTarget>();
		depthStencil = make_shared<DepthStencil>();

		Resize(mapSize);
	}

	const CComPtr<ID3D11ShaderResourceView>& ShadowMap::GetSRV() const
	{
		return renderTarget->srv;
	}

	const std::shared_ptr<Texture2D> ShadowMap::GetTexture()
	{
		return std::static_pointer_cast<Texture2D>(renderTarget);
	}

	///////////////////////////////////////////////////////////

	list<weak_ptr<Light>> Light::allLights;

	Light::Light()
	{
		shadowMapSize = GlobalSetting::graphicSetting.shadowMapSize;
		shadowShader = Resource::shaders.Find("VSMBuild");
		shadowTransform = Matrix::Identity;
	}

	void Light::Start()
	{
		shadowMap = make_shared<ShadowMap>();
		shadowMap->Initialize(shadowMapSize);
	}

	std::shared_ptr<Light> Light::CreateDirectionalLight()
	{
		auto go = GameObject::Instantiate("DirectionalLight");
		std::shared_ptr<Light> ptr = go->AddComponent<Light>();
		ptr->type = LightType::eDirectional;
		allLights.push_back(ptr);
		return ptr;
	}

	std::shared_ptr<Light> Light::CreatePointLight()
	{
		auto go = GameObject::Instantiate("DirectionalLight");
		std::shared_ptr<Light> ptr = go->AddComponent<Light>();
		ptr->type = LightType::ePoint;
		return ptr;
	}

	std::shared_ptr<Light> Light::CreateSpotLight()
	{
		auto go = GameObject::Instantiate("DirectionalLight");
		std::shared_ptr<Light> ptr = go->AddComponent<Light>();
		ptr->type = LightType::eSpot;

		return ptr;
	}

	LightData& Light::GetLightData()
	{
		data.position = GetTransform()->GetPosition();
		data.dir = GetTransform()->forward();
		data.lightType = type;
		data.color = lightColor;
		return data;
	}

	void Light::BuildShadowMap()
	{
		auto graphic = Graphic::Get();
		shadowMap->Bind(graphic);
		graphic->SetAlphaBlend(false);
		MeshRenderer::RenderAllToShadowMap(*this);
		graphic->SetBackBufferRenderTarget();
		graphic->SetAlphaBlend(true);
	}

	void Light::BuildShadowTransform()
	{		
		auto camWorld = Camera::main->GetTransform()->WorldMatrix();
		auto proj = Camera::main->GetCameraData().projMatrix;
		auto frustum = Frustum(proj);		
		frustum.Transform(camWorld);
		frustum.Far = shadowDistance;
		vector<Vector3> corners(frustum.CORNER_COUNT);
		frustum.GetCorners(&corners[0]);
		
		struct tempStruct
		{
			float len;
			float min;
			float max;
		};
		
		function<tempStruct(const Vector3&)> calc =
		[&](const Vector3& basis)->tempStruct {
			list<float> lenss;
			for(auto& corner : corners) {
				auto len = Vector3::Dot(basis, corner);
				lenss.push_back(len);
			}
			lenss.sort();
			tempStruct ret;
			ret.len = lenss.back() - lenss.front();
			ret.max = lenss.back();
			ret.min = lenss.front();
			return ret;
		};

		auto forward = GetTransform()->forward();
		auto right = GetTransform()->right();
		auto up = GetTransform()->up();

		tempStruct depthInfo = calc(forward);
		tempStruct widthInfo = calc(right);
		tempStruct heightInfo = calc(up);
		
		Vector3 viewPos = {		
			(widthInfo.max + widthInfo.min) * 0.5f,
			(heightInfo.max + heightInfo.min) * 0.5f,
			depthInfo.min
		};

		auto base = Matrix::Matrix(right, up, forward);

		viewPos = viewPos.Transform(base);
		
		cameraData.position = viewPos;
		auto focus = viewPos + forward;
		Matrix::LookAtLH(cameraData.position, focus, GetTransform()->up(), cameraData.viewMatrix);	
		Matrix::OrthoGraphicLH(widthInfo.len, heightInfo.len, 0.1f, depthInfo.len, cameraData.projMatrix);
		//Matrix::PerspectiveFovLH(MathUtil::ToRadians(45), 1, nearplane, depthInfo.len, cameraData.projMatrix);

		Matrix t = {
			0.5f, 0.0f, 0.0f, 0.0f,
			0.0f, -0.5f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.5f, 0.5f, 0.0f, 1.0f };

		shadowTransform = cameraData.viewMatrix * cameraData.projMatrix * t;
	}

	void Light::BuildShadowMaps()
	{
		for(auto& i = allLights.begin(); i != allLights.end(); i++) {
			if(i->expired())
				allLights.erase(i);
			else if(i->lock()->renderShadow) {
				i->lock()->BuildShadowTransform();
				i->lock()->BuildShadowMap();
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////
}