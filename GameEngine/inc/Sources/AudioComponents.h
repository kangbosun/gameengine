#pragma once

#pragma warning(push)
#pragma warning(disable:4251)
namespace GameEngine
{
	struct AudioClip;

	///////////////////////////////////////////////////////////////////////

	struct GAMEENGINE_API AudioListener : public Component
	{
	private:
		static std::weak_ptr<AudioListener> listener;
	public:
		static std::shared_ptr<AudioListener> GetListener();

		void OnEnable();
		void OnDisable();
	};

	/////////////////////////////////////////////////////////////////////////

	struct GAMEENGINE_API AudioSource 
		: public Cloneable<AudioSource, Component>
	{
	private :
		struct
		{
			float volume = 1;
			float pitch = 1;
			std::shared_ptr<AudioClip> currentClip;
		} playOption;

		struct
		{
			float maxDistance = 10;
			bool use3D = false;
		} option;

	public :
		std::unordered_map<std::string, std::shared_ptr<AudioClip>> clips;

		void Play(const std::string name);
		void Pause();
		void Stop();

		void SetVolume(float v);
		void SetPitch(float p);

		void SetMaxDistance(float d);
		void Set3DSound(bool b);

		bool NowPlaying();

		void AddClip(const std::shared_ptr<AudioClip>& clip);
		void RemoveClip(const std::string name);

		void SetReverb(bool b);

		void Update();
	};
}

#pragma warning(pop)
