#include "enginepch.h"
#include "DirectXAbstract.h"
#include "DebugClass.h"
#include "Texture2D.h"
#include "Graphic.h"
#include "DirectXTex\DirectXTex.h"
#pragma comment(lib, "DirectXTex.lib")

namespace GameEngine
{
	using namespace std;

	void Texture2D::LoadFromFile(const wstring & path, bool compressToDXT)
	{
		Debug::Log(L"#Load : " + path);
		isValid = false;
		auto graphic = Graphic::Get();


		wchar_t ext[_MAX_EXT];
		wchar_t fname[_MAX_FNAME];
		_wsplitpath_s(path.c_str(), 0, 0, 0, 0, fname, _MAX_FNAME, ext, _MAX_EXT);

		auto img = make_shared<DirectX::ScratchImage>();

		HRESULT hr = 0;
		if(_wcsicmp(ext, L".dds") == 0) {
			hr = DirectX::LoadFromDDSFile(path.c_str(), 0, 0, *img);
		}
		else if(_wcsicmp(ext, L".tga") == 0) {
			hr = DirectX::LoadFromTGAFile(path.c_str(), 0, *img);
		}
		else {
			hr = DirectX::LoadFromWICFile(path.c_str(), 0, 0, *img);
		}

		if(FAILED(hr)) {
			Debug::Failed("LoadFromXXXFile()");
			return;
		}

		if(compressToDXT) {
			auto temp = make_shared<DirectX::ScratchImage>();
			hr = Compress(img->GetImages(), img->GetImageCount(), img->GetMetadata(),
						  DXGI_FORMAT_BC3_UNORM, DirectX::TEX_COMPRESS_PARALLEL, 0.5f, *temp);
			if(FAILED(hr)) {
				Debug::Log("Failed to Compress", Debug::Yellow);
			}
			else {
				img = temp;
				temp.reset();
			}
		}

		hr = CreateTexture(graphic->GetDevice(), img->GetImages(), img->GetImageCount(), img->GetMetadata(), (ID3D11Resource**)&texture2D.p);
						
		if(FAILED(hr)) {
			Debug::Failed("CreateTexture()");
			return;
		}

		hr = graphic->GetDevice()->CreateShaderResourceView(texture2D, 0, &srv.p);
		if(FAILED(hr)) {
			Debug::Failed("CreateShaderResourceView()");
			return;
		}

		width = img->GetMetadata().width;
		height = img->GetMetadata().height;

		texture2D.Release();
		Debug::Success();
		isValid = true;
	}

	void Texture2D::CreateTexture2D(int width, int height, TextureFormat format, Usage usage, BindFlag bindFlag, CpuAccess cpuflag)
	{
		D3D11_TEXTURE2D_DESC texDesc;
		texDesc.Width = (UINT)width;
		texDesc.Height = (UINT)height;
		texDesc.MipLevels = 1;
		texDesc.ArraySize = 1;
		texDesc.Format = (DXGI_FORMAT)format;
		texDesc.SampleDesc.Count = msaa;
		texDesc.SampleDesc.Quality = 0;
		texDesc.Usage = (D3D11_USAGE)usage;
		texDesc.BindFlags = (UINT)bindFlag;
		texDesc.CPUAccessFlags = cpuflag;
		texDesc.MiscFlags = 0;

		auto graphic = Graphic::Get();

		HRESULT hr = graphic->GetDevice()->CreateTexture2D(&texDesc, 0, &texture2D.p);
		if(FAILED(hr)) {
			Debug::Failed("CreateTexture2D(" + ToString() + ")");
			return;
		}

		if(bindFlag & D3D11_BIND_SHADER_RESOURCE) {
			hr = graphic->GetDevice()->CreateShaderResourceView(texture2D, 0, &srv.p);
			if(FAILED(hr)) {
				Debug::Failed("CreateShaderResourceView(" + ToString() + ")");
				return;
			}
		}
		isValid = true;
	}
}