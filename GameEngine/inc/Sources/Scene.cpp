#include "enginepch.h"
#include "Scene.h"
#include "GameTime.h"
#include "GameObject.h"
#include "Renderer.h"
#include "Light.h"
#include "Camera.h"
#include "Graphic.h"

namespace GameEngine
{
	void Scene::Update()
	{
		GameObject::Root->UpdateHierarchy();
		Component::UpdateAll();
	}

	void Scene::Render()
	{
		Light::BuildShadowMaps();
		//meshRenderer
		MeshRenderer::RenderAll(Camera::main->GetCameraData());

		//skybox
		Camera::main->RenderSkybox();

		//ui Renderer
		Graphic::Get()->SetDepthEnable(false);
		UIRenderer::RenderAll(Camera::ui->GetCameraData());
		Graphic::Get()->SetDepthEnable(true);
	}
}