#include "enginepch.h"
#include "Audio.h"
#include "GameTime.h"

#define AL_ALEXT_PROTOTYPES
#include <AL\al.h>
#include <AL\alc.h>
#include <AL\alext.h>
#include <AL\efx-presets.h>

#pragma comment(lib, "OpenAL32.lib")

using namespace std;

namespace GameEngine
{
	AudioClip::AudioClip()
	{
		alGenSources(1, &sourceID);

		for(int i = 0; i < AUDIO_STREAM_BUFFER_COUNT; ++i) {
			unsigned int buffer;
			alGenBuffers(1, &buffer);
			bufferID.push_back(buffer);
		}
		
	}

	void AudioClip::QueueStartBuffer()
	{
		fileReader->SeekDataPos();
		auto info = fileReader->GetInfo();
		vector<char> buf(AUDIO_STREAM_BUFFER_SIZE);
		for(size_t i = 0; i < bufferID.size(); ++i) {
			auto len = fileReader->Read(buf);
			if(len > 0) {
				alBufferData(bufferID[i], info.format, &buf[0], (int)len, info.frequency);
				alSourceQueueBuffers(sourceID, 1, &bufferID[i]);
			}
			else break;
		}
	}


	AudioInfo AudioClip::GetInfo()
	{
		if(fileReader)
			return fileReader->GetInfo();
		else
			return AudioInfo();
	}

	void AudioClip::SetLoop(bool b)
	{
		loop = b;
	}

	bool AudioClip::GetLoop()
	{
		return loop;
	}

	AudioClip::AudioState AudioClip::GetState()
	{
		//int alState;
		//alGetSourcei(sourceID, AL_SOURCE_STATE, &alState);
		//if(alState == AL_PLAYING)
		//	state = AudioClip::Playing;
		//if(alState == AL_PAUSED)
		//	state = AudioClip::Paused;
		//if(alState == AL_STOPPED)
		//	state = AudioClip::Stopped;	
		return state;
	}

	void AudioClip::SetPitch(float value)
	{
		pitch = __max(value, 0);
		alSourcef(sourceID, AL_PITCH, pitch);
	}

	float AudioClip::GetPitch()
	{
		return pitch;
	}

	void AudioClip::SetVolume(float value)
	{
		volume = __max(value, 0);
		volume = __min(value, 1);
		alSourcef(sourceID, AL_GAIN, volume);
	}

	float AudioClip::GetVolume()
	{
		return volume;
	}

	void AudioClip::SetPos(float x, float y, float z)
	{
		alSource3f(sourceID, AL_POSITION, x, y, z);
	}

	void AudioClip::GetPos(float & xout, float& yout, float& zout)
	{
		alGetSource3f(sourceID, AL_POSITION, &xout, &yout, &zout);
	}

	void AudioClip::SetMaxDistance(float dist)
	{
		dist = max(0, dist);
		alSourcef(sourceID, AL_MAX_DISTANCE, dist);
	}

	float AudioClip::GetMaxDistance()
	{
		float dist;
		alGetSourcef(sourceID, AL_MAX_DISTANCE, &dist);
		return dist;
	}

	void AudioClip::Enable3DSound()
	{
		//alSourcei(sourceID, AL_SOURCE_RELATIVE, AL_FALSE);
	}

	void AudioClip::Disable3DSound()
	{
		alSourcei(sourceID, AL_SOURCE_RELATIVE, AL_TRUE);
		alSource3f(sourceID, AL_POSITION, 0, 0, 0);
	}

	void AudioClip::SetListenerPos(float x, float y, float z)
	{
		alListener3f(AL_POSITION, x, y, z);
	}

	void AudioClip::GetListenerPos(float & xout, float& yout, float& zout)
	{
		alGetListener3f(AL_POSITION, &xout, &yout, &zout);
	}


	void AudioClip::Stop()
	{
		alSourceStop(sourceID);

		int queued = 0;
		alGetSourcei(sourceID, AL_BUFFERS_QUEUED, &queued);
		while(queued--) {
			unsigned int buffer;
			alSourceUnqueueBuffers(sourceID, 1, &buffer);
		}
		state = AudioClip::Stopped;
	}

	void AudioClip::Pause()
	{
		alSourcePause(sourceID);
		state = AudioClip::Paused;
	}

	bool AudioClip::Play(float _pitch, float _volume)
	{
		if(state != AudioClip::Playing) {
			if(state == AudioClip::Stopped)
				QueueStartBuffer();
			SetPitch(_pitch);
			SetVolume(_volume);
			alSourcePlay(sourceID);
			state = AudioClip::Playing;
			return true;
		}
		else
			return false;
	}

	void AudioClip::Stream()
	{
		alSourcef(sourceID, AL_PITCH, pitch * GameTime::timeScale);
		int processed = 0;
		alGetSourcei(sourceID, AL_BUFFERS_PROCESSED, &processed);
		if(processed <= 0)
			return;

		auto info = fileReader->GetInfo();

		if(processed > 0) {
			vector<char> buf(AUDIO_STREAM_BUFFER_SIZE);
			do {
				ALuint buffer;
				alSourceUnqueueBuffers(sourceID, 1, &buffer);
				auto len = fileReader->Read(buf);
				if(len > 0) {
					alBufferData(buffer, info.format, &buf[0], (int)len, info.frequency);
					alSourceQueueBuffers(sourceID, 1, &buffer);
				}
				else break;
			} while(--processed);
		}

		int queued = 0;
		alGetSourcei(sourceID, AL_BUFFERS_QUEUED, &queued);
		if(queued == 0) {
			Stop();
			if(loop)
				Play();
		}
	}

	void AudioClip::SetEffect(const AudioEffect& efx) const
	{
		alSource3i(sourceID, AL_AUXILIARY_SEND_FILTER, efx.slotID, 0, AL_FILTER_NULL);
	}

	void AudioClip::UnsetEffect() const
	{
		alSource3i(sourceID, AL_AUXILIARY_SEND_FILTER, 0, 0, AL_FILTER_NULL);
	}

	
	std::shared_ptr<AudioClip> AudioClip::Clone()
	{
		if(fileReader) {
			return Audio::LoadFromFile(fileReader->GetPath().c_str());
		}
		return nullptr;
	}

	AudioClip::~AudioClip()
	{
		if(state = AudioClip::Playing)
			alSourceStop(sourceID);
		if(fileReader) {
			delete fileReader;
			fileReader = 0;
		}

		if(bufferID.size() > 0)
			alDeleteBuffers((int)bufferID.size(), &bufferID[0]);
		if(sourceID)
			alDeleteSources(1, &sourceID);
	}
}
