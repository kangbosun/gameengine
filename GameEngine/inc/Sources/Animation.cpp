#include "enginepch.h"
#include "Animation.h"
#include "GameObject.h"
#include "Renderer.h"
#include "GameTime.h"
#include "Transform.h"

namespace GameEngine
{
	using namespace std;
	

	void Animation::Play(const string& clipName)
	{
		run = false;
		if(!GetGameObject())
			return;

		auto pair = clips.find(clipName);
		if(pair != clips.cend()) {
			elaspedTime = 0;
			clip = pair->second;

			if(transforms.size() == 0) {
				// has transform curves;
				if(clip->transformCurves.size() > 0) {
					//find top object
					auto root = GetGameObject();
					while(root->GetParent())
						root = root->GetParent();
					//done

					transforms.reserve(clip->transformCurves.size());
					for(auto& curve : clip->transformCurves) {
						auto g = root->FindGameObjectInChildren(curve.boneName);
						transforms.push_back(&g->transform);
					}
				}
				else return;
			}
			state = Playing;
			run = true;
		}
	}

	void Animation::Pause()
	{
		run = false;
		state = Paused;
	}

	void Animation::Stop()
	{
		state = Stopped;
	}

	void Animation::Resume()
	{
		if(state == Paused) {
			run = true;
			state = Playing;
		}
	}

	void Animation::Update()
	{
		if(!clip || !run)
			return;

		elaspedTime += GameTime::deltaTime * speed;
		float endTime = clip->lengthInSeconds;
		float startTime = clip->startTime;

		while(elaspedTime >= endTime) {
			if(!loop) {
				elaspedTime = 0;
				run = false;
				return;
			}
			elaspedTime -= endTime;
		}

		if(elaspedTime < startTime)
			return;

		const size_t nBones = transforms.size();
		const size_t nClusters = clip->transformCurves.size();
		if(nBones != nClusters) {
			run = false;
			return;
		}

		if(state == Stopped) {
			elaspedTime = 0;
			run = false;
		}

		for(int i = 0; i < nBones; ++i) {
			auto& transformCurve = clip->transformCurves[i];
			auto& transform = transforms[i];
			//translation
			transform->SetPosition(transformCurve.EvaluateT(elaspedTime));
			//scaling
			transform->SetScale(transformCurve.EvaluateS(elaspedTime));
			//rotation
			transform->SetRotation(transformCurve.EvaluateR(elaspedTime));
		}
	}

	Animation::Animation(const Animation & rhs)
	{
		clips = rhs.clips;
		clip = nullptr;
		speed = rhs.speed;
		loop = rhs.loop;
		elaspedTime = 0;
		run = nullptr;
		state = Stopped;
	}
}