#pragma once

#pragma warning(push)
#pragma warning(disable:4251)

#ifdef _WIN32
#include "DirectXAbstract.h"
#endif

namespace GameEngine
{
	class RenderTarget;
	class DepthStencil;
	class Mesh;
	

	////////////////////////////////////////////////////

	class GAMEENGINE_API Graphic : public std::enable_shared_from_this<Graphic>
	{
	private:
		int id = 0;

		DXGI_SAMPLE_DESC sampleDesc;
		D3D11_FILL_MODE fillMode;

		D3D11_RASTERIZER_DESC rasterizerDesc;

		std::shared_ptr<RenderTarget> renderTarget;
		std::shared_ptr<DepthStencil> depthStencil;
		std::shared_ptr<RenderTarget> currRenderTarget;
		std::shared_ptr<DepthStencil> currDepthStencil;

		CComPtr<ID3D11DepthStencilState> depthStencilState;

		CComPtr<ID3D11RasterizerState> solidRasterState;
		CComPtr<ID3D11RasterizerState> wireRasterState;
		CComPtr<ID3D11RasterizerState> rasterizeState;

		CComPtr<ID3D11BlendState> blendState;
		CComPtr<ID3D11BlendState> disableBlendState;

		int videoMemory;
		std::wstring videoCardDesc;

		Viewport viewPort;

		float aspectRatio;
		Vector2 resolution;
		Vector2 UIScale;

		class GameWindow* window = nullptr;
		static std::unordered_map<int, std::shared_ptr<Graphic>> allGraphics;
		UINT vSync;

		CComPtr<GraphicDevice> device;
		CComPtr<GraphicDeviceContext> deviceContext;
		CComPtr<SwapChain> swapChain;
	public:
		float GetAspectRatio() { return aspectRatio; }
		Vector2 GetResolution() { return resolution; }
		Vector2 GetUIScale() { return UIScale; }

		static std::shared_ptr<Graphic> Get(int _id = 0);
		CComPtr<GraphicDevice> GetDevice() const { return device; }
		CComPtr<GraphicDeviceContext> GetDeviceContext() const { return deviceContext; }
	
		int GetID() { return id; }
		
		void Initialize(int width, int height, GameWindow* window);

		void Resize(int width, int height);

		void SetDepthEnable(bool b);
		void SetFillMode(D3D11_FILL_MODE mode);

		void ClearRenderTarget(const Color& color);
		void ClearDepthStencil();

		void SetRenderTarget(const std::shared_ptr<RenderTarget>& renderTarget);
		void SetRenderTarget(const std::shared_ptr<RenderTarget>& renderTarget, const std::shared_ptr<DepthStencil>& depthStencil);
		void SetBackBufferRenderTarget();
		void SetViewport(const Viewport& viewport);

		const CComPtr<ID3D11DepthStencilView>& GetDepthStencilView();
		void SwapBuffers();
		void SetCullMode(D3D11_CULL_MODE mode);

		int VMemory();
		std::wstring VideoCardName();

		void SetAlphaBlend(bool b);
		Vector2 GetViewportSize() { return Vector2(viewPort.Width, viewPort.Height); }

		int MSAA() { return sampleDesc.Count; }

		void SetVsync(bool b);
		bool GetVsync();

		void SetDSComparison(D3D11_COMPARISON_FUNC comparison);

		// Mesh
	public :
		std::shared_ptr<Mesh> CreateMesh(std::vector<Vertex>& vertices, std::vector<unsigned long>& indices, std::vector<UINT>* vertsOfSub = nullptr,
										 D3D11_PRIMITIVE_TOPOLOGY primitive = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST) const;
		void SetMeshBuffer(const std::shared_ptr<Mesh>& mesh) const;
	};
}
#pragma warning(pop)