#pragma once

#pragma warning(push)
#pragma warning(disable:4251)

struct ALCdevice_struct;
struct ALCcontext_struct;
struct OggVorbis_File;

#define AUDIO_STREAM_BUFFER_COUNT 5
#define AUDIO_STREAM_BUFFER_SIZE 1<<12 

namespace GameEngine
{
	struct GAMEENGINE_API AudioInfo
	{
		float duration = 0;
		unsigned int bitRate = 0;
		unsigned int frequency = 0;
		unsigned int channels = 0;
		unsigned int format = 0;
	};

	//abstract class
	struct GAMEENGINE_API AudioReader
	{
	protected:
		AudioInfo info;
		AudioReader() = default;
		std::string path;
	public:
		std::string GetPath() { return path; }
		virtual bool LoadFile(const char* path) = 0;
		AudioInfo GetInfo() { return info; }
		virtual void SeekDataPos() = 0;
		virtual size_t Read(std::vector<char>& buf) = 0;
		virtual ~AudioReader() = default;
		virtual AudioReader* Clone() = 0;
	};


	struct GAMEENGINE_API AudioReaderWAV : public AudioReader
	{
		friend struct AudioClip;
	protected:
		FILE* file = nullptr;
		size_t dataOffset = 0;
	public:
		virtual size_t Read(std::vector<char>& buf) override;
		virtual void SeekDataPos() override;
		virtual ~AudioReaderWAV();
		virtual bool LoadFile(const char * path) override;
		virtual AudioReader * Clone() override;
	};

	struct GAMEENGINE_API AudioReaderOGG : public AudioReader
	{
		friend struct AudioClip;
	protected:
		OggVorbis_File* file = nullptr;

	public:
		virtual size_t Read(std::vector<char>& buf) override;
		virtual void SeekDataPos() override;
		virtual ~AudioReaderOGG();
		virtual bool LoadFile(const char * path) override;
		virtual AudioReader * Clone() override;
	};

	///////////////////////////////////////////////////////

	struct GAMEENGINE_API AudioEffect
	{
		friend struct AudioClip;
		friend struct Audio;
	protected:
		unsigned int effectID = 0;
		unsigned int slotID = 0;
		std::string effectName;
		bool valid = false;

	public:
		bool IsValid() { return valid; }
		std::string GetName() { return effectName; }
	};

	struct GAMEENGINE_API AudioEffectReverb : public AudioEffect
	{
		AudioEffectReverb();
	};

	////////////////////////////////////////////////////////////

	struct GAMEENGINE_API AudioClip
	{
		friend struct Audio;
	public:
		enum AudioState
		{
			Playing, Paused, Stopped
		};


	protected:
		std::string name;
		unsigned int sourceID = 0;
		std::vector<unsigned int> bufferID;		
		AudioReader* fileReader = nullptr;
		AudioState state = AudioClip::Stopped;
		float pitch = 0;
		float volume = 0;
		bool valid = false;
		bool loop = false;
		bool reverb = false;
		
		//float position[3];

	protected:
		AudioClip();
		void QueueStartBuffer();
	public:
		std::string GetName() { return name; }

		AudioInfo GetInfo();

		void SetLoop(bool b);
		bool GetLoop();
		AudioState GetState();

		bool IsValid() { return valid; }

		void SetPitch(float value);
		float GetPitch();

		void SetVolume(float value);
		float GetVolume();

		void SetPos(float x, float y, float z);
		void GetPos(float& xout, float& yout, float& zout);

		void SetMaxDistance(float dist);
		float GetMaxDistance();

		void Enable3DSound();
		void Disable3DSound();

		static void SetListenerPos(float x, float y, float z);
		static void GetListenerPos(float& xout, float& yout, float& zout);

		bool Play(float pitch = 1, float volume = 1);
		void Stop();
		void Pause();
		void Stream();
		void SetEffect(const AudioEffect& efx) const;
		void UnsetEffect() const;

		std::shared_ptr<AudioClip> Clone();
		~AudioClip();
	};



	struct GAMEENGINE_API Audio
	{
		friend class GameWindow;
	private:
		ALCdevice_struct* device;
		ALCcontext_struct* context;
		bool paused;

		static std::shared_ptr<AudioClip> LoadFromWAV(const char* path);
		static std::shared_ptr<AudioClip> LoadFromOGG(const char* path);
		bool Init();
		void Destroy();

		Audio() = default;
		~Audio() = default;

	public:	
		static struct AudioEffects
		{
			std::shared_ptr<AudioEffect> reverb;
		} effects;

		void PauseDevice();
		void ResumeDevice();
		static std::shared_ptr<AudioClip> LoadFromFile(const char* path);
	};
}
#pragma warning(pop)