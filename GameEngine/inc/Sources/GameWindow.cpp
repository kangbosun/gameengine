#include "enginepch.h"

#include "GameWindow.h"
#include "DebugClass.h"
#include "Camera.h"
#include "Resource.h"
#include "Graphic.h"
#include "Input.h"
#include "Light.h"
#include "GameObject.h"
#include "GameTime.h"
#include "Scene.h"
#include "GlyphPool.h"
#include "Audio.h"

#include <thread>
#include <mmsystem.h>

#pragma comment(lib, "winmm.lib")

namespace GameEngine
{
	using namespace std;
	using namespace std::chrono_literals;

	unordered_map<int, shared_ptr<GameWindow>> GameWindow::allWindows;

	LRESULT CALLBACK GameWindow::static_WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		for(auto& wnd : allWindows) {
			if(wnd.second)
				return wnd.second->WndProc(hWnd, msg, wParam, lParam);
		}
		return 0;
	}

	LRESULT GameWindow::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		static bool ncldown = false;
		switch(msg) {
			case WM_SIZE:
			{
				if(wParam == SIZE_MINIMIZED) {
					audio->ResumeDevice();
					ncldown = false;
					return DefWindowProc(hWnd, msg, wParam, lParam);
				}
				else if(wParam == SIZE_MAXIMIZED || wParam == SIZE_RESTORED) {
					audio->ResumeDevice();
					ncldown = false;
				}
				RECT rect;
				GetClientRect(hWnd, &rect);

				width = rect.right - rect.left;
				height = rect.bottom - rect.top;
				
				positionX = rect.left;
				positionY = rect.top;
				return 0;
			}

			case WM_MOUSEWHEEL :
			{
				float delta = GET_WHEEL_DELTA_WPARAM(wParam);
				Input::mouseWheelDelta = delta;
				return 0;
			}


			case WM_NCLBUTTONDOWN :
			{
				audio->PauseDevice();
				ncldown = true;
				return DefWindowProc(hWnd, msg, wParam, lParam);
			}
			case WM_NCMOUSEMOVE : // WM_NCLBUTTONUP doesn't work
			{
				if(ncldown) {
					audio->ResumeDevice();
					GameTime::Update();
					ncldown = false;
				}
				return DefWindowProc(hWnd, msg, wParam, lParam);
			}

			case WM_DESTROY:
				PostQuitMessage(0);
				return 0;
			default:
				return DefWindowProc(hWnd, msg, wParam, lParam);
		}
	}

	bool GameWindow::Initialize(LPCWSTR title, int width, int height, int framePerSecond)
	{
#ifdef _DEBUG
		AllocConsole();
		freopen("CONOUT$", "w", stdout);
#endif
		WNDCLASS wc = { 0 };
		wc.style = CS_OWNDC;
		wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wc.lpszClassName = (title);
		wc.lpfnWndProc = static_WndProc;

		this->width = width;
		this->height = height;

		RECT rect = { 0, 0, width, height };
		AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW | WS_VISIBLE, false);

		if(!RegisterClassW(&wc)) {
			MessageBox(NULL, _T("Failed to Register Class"), _T("Error"), MB_OK);
			return false;
		}

		//init audio device
		audio = shared_ptr<Audio>(new Audio());
		audio->Init();

		hWnd = CreateWindowW(wc.lpszClassName, title,
							 WS_OVERLAPPEDWINDOW | WS_VISIBLE,
							 CW_USEDEFAULT, CW_USEDEFAULT, rect.right - rect.left, rect.bottom - rect.top,
							 NULL, NULL, NULL, this);

		if(hWnd == NULL) {
			MessageBox(NULL, _T("Failed to Create Window"), _T("Error"), MB_OK);
			return false;
		}

		//init graphic device
		graphic = make_shared<Graphic>();
		graphic->Initialize(width, height, this);

		this->framePerSecond = max(1, framePerSecond);
		this->updatePerSecond = this->framePerSecond;

		frameTime = 1000.0f / (float)this->framePerSecond / 1000.0f;
		updateTime = 1000.0f / (float)this->updatePerSecond / 1000.0f;

		elaspedTime = 0;

		Camera::main = Camera::CreateCamera(Camera::ProjMode::ePerspective);
		Camera::ui = Camera::CreateCamera(Camera::ProjMode::eOrtho);

		//load library resources
		Resource::LoadDefaultResource();

		GlyphPool::GetInstance();
		
		timeBeginPeriod(1);

		return true;
	}

	int GameWindow::Run(int fps)
	{
		running = true;
			
		while(hWnd) {
			auto start = chrono::high_resolution_clock::now();
			MSG msg = { 0 };
			if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
				if(msg.message == WM_QUIT) {
					Destroy();
					break;
				}
			}
			else {
				Update();
				//rendering
				graphic->ClearRenderTarget(Color::CornflowerBlue);
				graphic->ClearDepthStencil();
				Render();
		
				//swap buffers
				graphic->SwapBuffers();
				Input::mouseWheelDelta = 0;
			}
		}
		return 1;
	}

	void GameWindow::Update()
	{
		GameTime::Update();
		elaspedTime += GameTime::unscaledDeltaTime;
		if(elaspedTime >= updateTime) {
			GameTime::StopwatchStart(333);
			Input::Update(width, height, hWnd);
			if(currScene)
				currScene->Update();
			elaspedTime -= GameTime::unscaledDeltaTime;
			GameTime::updateTime = GameTime::StopwatchStop(333);
		}
	}

	void GameWindow::Render()
	{
		GameTime::StopwatchStart(333);
		if(currScene)
			currScene->Render();
		GameTime::frameTime = GameTime::StopwatchStop(333);
	}

	shared_ptr<GameWindow> GameWindow::Get(int _id)
	{
		auto f = allWindows.find(_id);
		if(f != allWindows.cend())
			return f->second;
		return nullptr;
	}

	void GameWindow::LoadScene(const shared_ptr<Scene>& scene)
	{
		if(!scene)
			return;
		UnloadScene();
		currScene = scene;
		currScene->OnLoad();
	}

	void GameWindow::UnloadScene()
	{
		if(currScene)
			currScene->OnUnload();
	}

	void GameWindow::Destroy()
	{
		audio->Destroy();
		audio.reset();
		allWindows.erase(id);
		timeEndPeriod(1);
	}

	std::shared_ptr<GameWindow> GameWindow::Create(const wstring & title, int width, int height, int framePerSec)
	{
		auto wnd = shared_ptr<GameWindow>(new GameWindow());
		wnd->id = (int)allWindows.size();
		allWindows[wnd->id] = wnd;
		if(!wnd->Initialize(title.c_str(), width, height, framePerSec)) {
			allWindows.erase(wnd->id);
			wnd.reset();
		}
		return wnd;
	}
}