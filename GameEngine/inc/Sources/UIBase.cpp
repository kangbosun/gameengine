#include "enginepch.h"
#include "UI.h"
#include "Renderer.h"
#include "Camera.h"
#include "Transform.h"
#include "GameObject.h"

namespace GameEngine
{
	bool operator <(const std::weak_ptr<UIBase>& lhs, const std::weak_ptr<UIBase>& rhs)
	{
		return (lhs.lock()->GetTransform()->worldPosition().z < rhs.lock()->GetTransform()->worldPosition().z);
	}

	const std::shared_ptr<UIRenderer> UIBase::GetRenderer()
	{
		using namespace std;

		if(_renderer.expired()) {
			auto go = GetGameObject();
			if(go) {
				_renderer = go->GetComponent<UIRenderer>();
				if(_renderer.expired())
					_renderer = go->AddComponent<UIRenderer>();
			}
		}
		return _renderer.lock();
	}

	void UIBase::SetColor(const Color& _color)
	{
		color = _color;
		GetRenderer()->material.data.color = color;
	}
}