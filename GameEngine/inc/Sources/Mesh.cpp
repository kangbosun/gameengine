#include "enginepch.h"
#include "Graphic.h"
#include "Mesh.h"
#include "DebugClass.h"

namespace GameEngine
{
	inline UINT Mesh::GetSubMeshCount(UINT index)
	{
		return vertCountOfSubMesh[index];
	}
}