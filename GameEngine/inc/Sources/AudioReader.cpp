#pragma once
#include "enginepch.h"
#include "Audio.h"
#include <AL\al.h>
#include <AL\alc.h>
#pragma comment(lib, "OpenAL32.lib")

#include <vorbis\vorbisfile.h>
#pragma comment(lib, "libvorbisfile.lib")

namespace GameEngine
{
	struct WAVE_DESC
	{
		struct
		{
			unsigned char chunkID[4];
			unsigned long chunkSize;
			unsigned char format[4];
		} RIFF;

		struct
		{
			unsigned char chunkID[4];
			unsigned long chunkSize;
			unsigned short audioFormat;
			unsigned short numChannels;
			unsigned long sampleRate;
			unsigned long byteRate;
			unsigned short blockAlign;
			unsigned short bitsPerSample;
		} Format;

		struct
		{
			char chunkID[4];
			unsigned long dataSize;
		} Data;

		bool IsValid()
		{
			bool briff = RIFF.chunkID[0] == 'R' && RIFF.chunkID[1] == 'I' &&
				RIFF.chunkID[2] == 'F' && RIFF.chunkID[3] == 'F';
			bool bwave = RIFF.format[0] == 'W' && RIFF.format[1] == 'A' &&
				RIFF.format[2] == 'V' && RIFF.format[3] == 'E';
			bool bfmt = Format.chunkID[0] == 'f' && Format.chunkID[1] == 'm' &&
				Format.chunkID[2] == 't' && Format.chunkID[3] == ' ';
			bool bdata = Data.chunkID[0] == 'd' && Data.chunkID[1] == 'a' &&
				Data.chunkID[2] == 't' && Data.chunkID[3] == 'a';

			return briff && bwave && bfmt && bdata;
		}

		ALuint GetFormat()
		{
			ALuint format;
			if(Format.bitsPerSample == 16) {
				if(Format.numChannels == 1)
					format = AL_FORMAT_MONO16;
				else if(Format.numChannels == 2)
					format = AL_FORMAT_STEREO16;
			}
			else if(Format.bitsPerSample == 8) {
				if(Format.numChannels == 1)
					format = AL_FORMAT_MONO8;
				else if(Format.numChannels == 2)
					format = AL_FORMAT_STEREO8;
			}
			return format;
		}
	};

	////////////////////////
	// wav reader
	////////////////////////

	size_t AudioReaderWAV::Read(std::vector<char>& buf)
	{
		if(file == nullptr)
			return 0;
		else
			return fread(&buf[0], 1, buf.size(), file);
	}

	void AudioReaderWAV::SeekDataPos()
	{
		if(file)
			fseek(file, (int)dataOffset, SEEK_SET);
	}

	AudioReaderWAV::~AudioReaderWAV()
	{
		if(file) {
			fclose(file);
			file = nullptr;
		}
	}

	bool AudioReaderWAV::LoadFile(const char * path)
	{
		fopen_s(&file, path, "rb");
		if(file == nullptr) { // error!
			return false;
		}
		this->path = path;
		WAVE_DESC desc;
		fread(&desc, sizeof(WAVE_DESC), 1, file);
		if(desc.IsValid() == false) { // error
			return false;
		}
		auto format = desc.GetFormat();
		info.bitRate = desc.Format.byteRate << 3;
		info.channels = desc.Format.numChannels;
		info.duration = (float)((double)desc.Data.dataSize / (double)desc.Format.byteRate);
		info.frequency = desc.Format.sampleRate;
		info.format = format;
		dataOffset = sizeof(WAVE_DESC);
		return true;
	}

	AudioReader * AudioReaderWAV::Clone()
	{
		auto c = new AudioReaderWAV();
		c->LoadFile(path.c_str());
		return c;
	}

	////////////////////////
	// ogg reader
	////////////////////////

	using namespace std;

	size_t AudioReaderOGG::Read(std::vector<char>& buf)
	{
		if(file) {
			size_t totalSize = buf.size();
			auto blockSize = totalSize >> 3;
			size_t len = 0;
			do {
				int bitStream;
				auto bytes = ov_read(file, &buf[0] + len, (int)(totalSize - len), 0, 2, 1, &bitStream);
				if(bytes < 1)
					break;
				len += bytes;
			} while(len < totalSize);
			return len;
		}
		return 0;
	}

	void AudioReaderOGG::SeekDataPos()
	{
		if(file) {
			ov_pcm_seek(file, 0);
		}
	}

	AudioReaderOGG::~AudioReaderOGG()
	{
		ov_clear(file);
		file = nullptr;
	}

	bool AudioReaderOGG::LoadFile(const char * path)
	{
		if(file)
			ov_clear(file);

		file = new OggVorbis_File();
		int err = ov_fopen(path, file);
		if(err != 0)
			return false;
		auto vorbisInfo = ov_info(file, -1);

		info.channels = vorbisInfo->channels;
		info.bitRate = vorbisInfo->bitrate_nominal;
		info.frequency = vorbisInfo->rate;
		info.duration = (float)ov_time_total(file, -1);
		if(info.channels == 1)
			info.format = AL_FORMAT_MONO16;
		else if(info.channels == 2)
			info.format = AL_FORMAT_STEREO16;

		return false;
	}
	AudioReader * AudioReaderOGG::Clone()
	{
		auto c = new AudioReaderOGG();
		c->LoadFile(path.c_str());
		return c;
	}
}
