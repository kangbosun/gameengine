#pragma once
#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_

#pragma warning(push)
#pragma warning(disable:4251)

namespace GameEngine
{
	class Texture2D;
	class Transform;
	class Renderer;

	class GAMEENGINE_API GameObject final : public Object, public std::enable_shared_from_this<GameObject>
	{
		friend class Scene;
#pragma region Management
	private :
		static std::unordered_multimap<std::string, std::weak_ptr<GameObject>> allGameObjects;
		bool isRegistered = false;
	public :
		static void Register(const std::shared_ptr<GameObject>& go);
		static std::shared_ptr<GameObject> FindGameObject(const std::string& name);
		static void FindGameObjects(const std::string& name, std::vector<std::shared_ptr<GameObject>>& result);
#pragma endregion

#pragma region Hierarchy
	private :
		std::list<std::shared_ptr<GameObject>> children;
		std::shared_ptr<GameObject> parent;
		static std::shared_ptr<GameObject> Root;

		void UpdateHierarchy();

	public :	
		std::shared_ptr<GameObject> GetParent() { return parent; }
		void SetParent(const std::shared_ptr<GameObject>& go);
		const std::list<std::shared_ptr<GameObject>>& GetChildren() const { return children; }
		size_t GetChildCount() { return children.size(); }
	
		std::shared_ptr<GameObject> FindGameObjectInChildren(const std::string& name);
		void FindGameObjectsInChildren(const std::string& name, std::vector<std::shared_ptr<GameObject>>& result);

		

#pragma endregion
	private :
		bool active = true;
		bool destroyed = false;
	public:
		std::string name;

		Transform transform;
		
		// access renderer directly
	private :
		std::weak_ptr<Renderer> _renderer;
	public :
		std::shared_ptr<Renderer> GetRenderer();

	private :
		GameObject(const GameObject& gameObject);
		GameObject();
		/* don't try implicit copy */
		GameObject& operator=(const GameObject& rhs);
	public :
		void SetActive(bool b, bool recursively = true);
		bool GetActive() { return active; }
		std::shared_ptr<GameObject> CloneShared();

	public :
		static std::shared_ptr<GameObject> Instantiate(const std::string& name = "");
		static std::shared_ptr<GameObject> Instantiate(const std::shared_ptr<GameObject>& go);
		static void Destroy(std::shared_ptr<GameObject>& object);

#pragma region Component management methods
	private:
		std::vector<std::shared_ptr<Component>> components;

	public :
		template <typename T>
		std::shared_ptr<T> AddComponent();
		
		std::shared_ptr<Component> AddComponent(const std::shared_ptr<Component>& _com);

		template <typename T>
		std::shared_ptr<T> GetComponent();

		std::shared_ptr<Component> GetComponent(const std::string& name);

		template <typename T>
		std::shared_ptr<T> GetComponentInChildren();

		template <typename T>
		void RemoveComponent();

		void RemoveComponent(const std::string& name);
#pragma endregion
	};


#pragma region Template Methods Definitions
	template <typename T>
	std::shared_ptr<T> GameObject::AddComponent()
	{
		auto temp = std::make_shared<T>();
		AddComponent(std::static_pointer_cast<Component>(temp));
		return temp;
	}

	template <typename T>
	std::shared_ptr<T> GameObject::GetComponent()
	{
		std::shared_ptr<T> target;
		for(auto& com : components) {
			target = std::dynamic_pointer_cast<T>(com);
			if(target)
				break;
		}
		return target;
	}
	
	template <typename T>
	std::shared_ptr<T> GameObject::GetComponentInChildren()
	{
		std::shared_ptr<T> target = GetComponent<T>();

		if(!target) {
			for(auto i = children.begin(); i != children.cend(); ++i) {
				auto child = *i;
				if(child) 
					target = child->GetComponentInChildren<T>();
				if(target) break;
			}
		}
		return target;
	}

	template <typename T>
	void GameObject::RemoveComponent()
	{
		for(int i = 0; i < components.size(); ++i) {
			auto target = std::dynamic_cast<T*>(components[i]);
			if(target) {
				swap(components[i], components.back());
				components.pop_back();
				return;
			}
		}
	}
#pragma endregion

}

#pragma warning(pop)
#endif