#pragma once

#pragma warning(push)
#pragma warning(disable:4251)

namespace GameEngine
{
	struct GAMEENGINE_API GraphicSetting
	{
		Color ambientColor = Color(0.1f, 0.1f, 0.1f, 1.0f);
		int shadowMapSize = 2048;
	};

	struct GAMEENGINE_API GlobalSetting
	{
		static std::string defaultFont;
		static GraphicSetting graphicSetting;
	};
}

#pragma warning(pop)