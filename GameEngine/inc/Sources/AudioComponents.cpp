#pragma once
#include "enginepch.h"
#include "Audio.h"
#include "AudioComponents.h"
#include "GameObject.h"
#include "DebugClass.h"

namespace GameEngine
{
	void AudioSource::Play(const std::string name)
	{
		if(playOption.currentClip) {
			if(playOption.currentClip->GetState() == AudioClip::Playing)
				playOption.currentClip->Stop();
		}
		auto iter = clips.find(name);
		if(iter != clips.cend()) {
			iter->second->Play(playOption.volume, playOption.pitch);
			playOption.currentClip = iter->second;
			if(option.use3D)
				playOption.currentClip->Enable3DSound();
			else
				playOption.currentClip->Disable3DSound();
			playOption.currentClip->SetMaxDistance(option.maxDistance);
		}
	}
	void AudioSource::Pause()
	{
		if(playOption.currentClip) {
			playOption.currentClip->Pause();
		}
	}
	void AudioSource::Stop()
	{
		if(playOption.currentClip) {
			playOption.currentClip->Stop();
			playOption.currentClip = nullptr;
		}
	}
	void AudioSource::SetVolume(float v)
	{
		v = min(1, max(0, v));
		playOption.volume = v;
		if(playOption.currentClip) {
			playOption.currentClip->SetVolume(v);
		}
	}
	void AudioSource::SetPitch(float p)
	{
		p = max(0, p);
		playOption.pitch = p;
		if(playOption.currentClip) {
			playOption.currentClip->SetPitch(p);
		}
	}

	void AudioSource::SetMaxDistance(float d)
	{
		d = max(0.0001f, d);
		option.maxDistance = d;
		if(playOption.currentClip) {
			playOption.currentClip->SetMaxDistance(d);
		}
	}
	void AudioSource::Set3DSound(bool b)
	{
		option.use3D = b;
		if(b) {
			if(playOption.currentClip) {
				playOption.currentClip->Enable3DSound();
				playOption.currentClip->SetMaxDistance(option.maxDistance);
			}
		}
		else {
			if(playOption.currentClip) {
				playOption.currentClip->Disable3DSound();
				playOption.currentClip->SetVolume(playOption.volume);
			}
		}
	}

	bool AudioSource::NowPlaying()
	{
		if(playOption.currentClip) {
			return playOption.currentClip->GetState() == AudioClip::Playing;
		}
		return false;
	}
	void AudioSource::AddClip(const std::shared_ptr<AudioClip>& clip)
	{
		if(clip) {
			clips[clip->GetName()] = clip;
		}
	}
	void AudioSource::RemoveClip(const std::string name)
	{
		clips.erase(name);
	}

	void AudioSource::SetReverb(bool b)
	{
		if(playOption.currentClip) {
			if(b)
				playOption.currentClip->SetEffect(*Audio::effects.reverb.get());
			else
				playOption.currentClip->UnsetEffect();
		}
	}

	void AudioSource::Update()
	{
		auto& clip = playOption.currentClip;

		if(clip && clip->GetState() == AudioClip::Playing) {
			auto listener = AudioListener::GetListener();
			if(!listener)
				return;

			// OpenAL supports 3D-Sound for mono channel only
			float gain = 1;
			if(option.use3D) {
				if(clip->GetInfo().channels > 1) {
					Vector3 v1 = GetTransform()->GetPosition();
					Vector3 v2 = listener->GetTransform()->GetPosition();
					float distance = (v2 - v1).Length();
					gain = 1.0f - (distance / option.maxDistance);
					// apply exponentil attenuation
					gain = exp(5 * gain);
					gain *= playOption.volume;
					clip->SetVolume(gain);				
				}				
			}
			playOption.currentClip->Stream();
		}
	}

	///////////////////////////////////////////////////////

	std::weak_ptr<AudioListener> AudioListener::listener;

	std::shared_ptr<AudioListener> AudioListener::GetListener()
	{
		return listener.lock();
	}
	void AudioListener::OnEnable()
	{
		if(!listener.expired()) {
			Debug::Log("Too many AudioListener (max = 1)", Debug::Yellow);
			
		}	
		else {
			listener = gameObject->GetComponent<AudioListener>();
		}
	}
	void AudioListener::OnDisable()
	{
		listener.reset();
	}

}