#pragma once

#pragma warning(push)
#pragma warning(disable:4251)

namespace GameEngine
{
	class GameObject;
	class Renderer;
	class Transform;

	class GAMEENGINE_API Component : public Cloneable<Component, Object>
	{
		friend class Scene;
		friend class GameObject;

	protected :
		bool registered = false;
		bool destroy = false;
		std::shared_ptr<GameObject> gameObject;
		bool enabled = true;
	public:	
		inline std::shared_ptr<GameObject> GetGameObject() { return gameObject; }
		Transform* const GetTransform();
		const std::shared_ptr<Renderer> GetRenderer();
		
	protected:
		virtual void OnDestroy() {}
		virtual void Start() {}
	public:	
		virtual void Update() {}
		virtual void OnCollisionEnter() {}
		virtual void OnCollisionExit() {}
		virtual void OnEnable() {}
		virtual void OnDisable() {}

		void SetEnable(bool b);

		Component();
		virtual ~Component();
		Component(const Component& c) { enabled = c.enabled; }


	private :
		static std::vector<std::weak_ptr<Component>> allComponents;
		static std::list<std::weak_ptr<Component>> watingForRegister;

		static void Register(const std::shared_ptr<Component>& com);
		static void UpdateAll();
		static void Clear() { allComponents.clear(); }
	};
}

#pragma warning(pop)