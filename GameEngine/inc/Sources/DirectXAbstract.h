
#pragma once

#include <d3d11.h>

namespace GameEngine
{
	typedef D3D11_VIEWPORT Viewport;
	typedef ID3D11Device GraphicDevice;
	typedef ID3D11DeviceContext GraphicDeviceContext;
	typedef IDXGISwapChain SwapChain;
}