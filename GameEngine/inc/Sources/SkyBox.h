#pragma once

#pragma warning(push)
#pragma warning(disable:4251)

namespace GameEngine
{
	class Shader;
	class Mesh;
	class Texture2D;
	struct CameraData;

	class GAMEENGINE_API Skybox : public Cloneable<Skybox, Component>
	{
		friend class Camera;
	protected:
		std::shared_ptr<Shader> skyShader;
		std::shared_ptr<Mesh> mesh;
		std::shared_ptr<Texture2D> skyMap;
		float size = 300;
	public:
		Skybox();
		Skybox(const Skybox& rhs);
		void SetSize(float _size) { size = _size; }
		virtual void Draw(const CameraData& cam);
	};
}

#pragma warning(pop)