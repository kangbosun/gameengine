#pragma once

#pragma warning(push)
#pragma warning(disable:4251)

namespace GameEngine
{
	namespace Math
	{
		struct AnimClip;
		struct AnimCurve;
	}

	class SkinnedMeshRenderer;
	class Transform;

	class GAMEENGINE_API Animation : public Cloneable<Animation, Component>
	{
	public :
		enum AnimState { Playing, Paused, Stopped };
	private:
		float elaspedTime = 0;
		std::vector<Transform*> transforms;
		AnimState state = Stopped;
		bool run = false;
	public:
		std::unordered_map<std::string, std::shared_ptr<AnimClip>> clips;
		std::shared_ptr<AnimClip> clip = nullptr;

		float speed = 1.0f;
		bool loop = true;
		
		AnimState GetState() { return state; }
		void Play(const std::string& clipName);
		void Pause();
		void Stop();
		void Resume();
		void Update();

		Animation() = default;
		Animation(const Animation& rhs);
	};
}

#pragma warning(pop)