#include "enginepch.h"

#include "Component.h"
#include "GameObject.h"
#include "Renderer.h"
#include "Transform.h"

namespace GameEngine
{
	using namespace std;

	template<typename T>
	vector<T> make_reserved_vector(size_t n)
	{
		vector<T> ret;
		ret.reserve(n);
		return ret;
	}

	vector<weak_ptr<Component>> Component::allComponents = make_reserved_vector<weak_ptr<Component>>(1000);
	list<weak_ptr<Component>> Component::watingForRegister;

	void Component::SetEnable(bool b)
	{
		enabled = b;
		if(enabled)
			OnEnable();
		else
			OnDisable();
	}

	Component::Component()
	{

	}

	Component::~Component()
	{
		OnDestroy();
	}

	void Component::Register(const shared_ptr<Component>& com)
	{
		if(!com->registered && com->GetGameObject()) {
			watingForRegister.push_back(com);
			//allComponents.push_back(com);
			com->registered = true;
			com->Start();
			if(com->enabled)
				com->SetEnable(true);
		}
	}


	void Component::UpdateAll()
	{
		vector<std::weak_ptr<Component>> forSwap;		
		forSwap.reserve(allComponents.size());

		for(int i = 0; i < allComponents.size(); ++i) {
			auto& weak = allComponents[i];
			if(!weak.expired()) {
				auto& strong = weak.lock();
				if(strong->registered) {
					if(strong->enabled && strong->GetGameObject()->GetActive())
						strong->Update();
					forSwap.emplace_back(move(weak));
				}
			}
		}

		forSwap.insert(forSwap.end(), watingForRegister.begin(), watingForRegister.end());
		watingForRegister.clear();

		allComponents.swap(forSwap);
		forSwap.clear();	
	}

	Transform* const Component::GetTransform()
	{
		auto go = GetGameObject();
		if(go)
			return &go->transform;
		else
			return nullptr;
	}

	const std::shared_ptr<Renderer> Component::GetRenderer()
	{
		auto go = GetGameObject();
		if(go)
			return go->GetRenderer();
		else
			return std::shared_ptr<Renderer>();
	}
}


