#pragma once
#include "enginepch.h"
#include "Audio.h"
#define AL_ALEXT_PROTOTYPES
#include <AL\al.h>
#include <AL\alc.h>
#include <AL\alext.h>
#include <AL\efx-presets.h>

#include "DebugClass.h"

#pragma comment(lib, "OpenAL32.lib")

using namespace std;

namespace GameEngine
{
	Audio::AudioEffects Audio::effects;

	bool Audio::Init()
	{
		if(device == nullptr) {
			//if(alcIsExtensionPresent(0, "ALC_ENUMERATE_ALL_EXT") == AL_TRUE) {
			//	auto dev = alcGetString(0, ALC_DEVICE_SPECIFIER);
			//	cout << "Audio Device Lists" << endl;
			//	while(*dev) {
			//		cout << dev << endl;
			//		dev += strlen(dev) + 1;
			//	}
			//}

			device = alcOpenDevice(NULL);
			if(device) {
				context = alcCreateContext(device, NULL);
				alcMakeContextCurrent(context);
			}
			
			Debug::Log(alcGetString(device, ALC_ALL_DEVICES_SPECIFIER)+1);
		}

		float dir[] = { 0, 0, -1, 0, 1, 0 };
		alListenerfv(AL_ORIENTATION, dir);

		effects.reverb = make_shared<AudioEffectReverb>();

		if(device && context) {
			return true;
		}

		else
			return false;
	}

	void Audio::Destroy()
	{
		if(context) {
			alcMakeContextCurrent(nullptr);
			alcDestroyContext(context);
			context = nullptr;
		}
		if(device) {
			alcCloseDevice(device);
			device = nullptr;
		}
	}

	void Audio::PauseDevice()
	{
		if(device) {
			if(paused == false) {
				alcDevicePauseSOFT(device);
				paused = true;
			}
		}
	}

	void Audio::ResumeDevice()
	{
		if(device) {
			if(paused) {
				alcDeviceResumeSOFT(device);
				paused = false;
			}
		}
	}



	std::shared_ptr<AudioClip> Audio::LoadFromWAV(const char * path)
	{
		auto clip = shared_ptr<AudioClip>(new AudioClip());

		auto reader = new AudioReaderWAV();
		if(!reader->LoadFile(path))
			false;

		clip->fileReader = reader;

		return clip;
	}

	std::shared_ptr<AudioClip> Audio::LoadFromOGG(const char * path)
	{
		auto clip = shared_ptr<AudioClip>(new AudioClip());

		auto reader = new AudioReaderOGG();
		if(!reader->LoadFile(path))
			false;

		clip->fileReader = reader;

		return clip;
	}

	std::shared_ptr<AudioClip> Audio::LoadFromFile(const char * path)
	{
		char ext[5];
		char name[20];
		_splitpath_s(path, 0, 0, 0, 0, name, sizeof(char) * 20, ext, sizeof(char) * 5);
		shared_ptr<AudioClip> ret;
		if(strcmp(ext, ".wav") == 0) {
			ret = LoadFromWAV(path);
		}
		else if(strcmp(ext, ".ogg") == 0) {
			ret = LoadFromOGG(path);
		}
		if(ret)
			ret->name = name;
		return ret;
	}
}