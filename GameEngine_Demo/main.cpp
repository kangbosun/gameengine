
#include <GameEngine.h>
//

using namespace std;
using namespace GameEngine;

#include "Grid.h"
#include "KeyCam.h"


class Rot : public Cloneable<Rot, Component>
{
public:
	Vector3 axis = Vector3::Z;
	float speed = 15;
	void Update()
	{
		GetTransform()->Rotate(axis, speed * GameTime::deltaTime);
	}
};

class FrameCounter : public Cloneable<FrameCounter, Component>
{
public:
	weak_ptr<Text> text;
	float elasped;
	float refreshTime = 1;
	void Start()
	{
		elasped = refreshTime;
		text = GetGameObject()->GetComponent<Text>();
	}

	void Update()
	{
		elasped += GameTime::unscaledDeltaTime;
		if(elasped > refreshTime) {
			int frames = (int) (1.0f / GameTime::unscaledDeltaTime);
			text.lock()->SetText(L"FPS : " + to_wstring(frames));
			elasped -= refreshTime;
		}
	}
};


class MousePos : public Cloneable<MousePos, Component>
{
public:
	weak_ptr<Text> text;

	void Start()
	{
		text = GetGameObject()->GetComponent<Text>();
	}

	void Update()
	{
		auto aa = text.lock()->GetText();
		auto mousepos = Input::GetMousePos();
		auto uimpos = UIInputManager::ConvertMousePos(mousepos);
		wstring str = L"x : " + to_wstring((int)uimpos.x) + L'\n';
		str += L"y : " + to_wstring((int)uimpos.y);
		text.lock()->SetText(str);
	}
};

class Scene01 : public Scene
{
public:
	shared_ptr<GameObject> talia, attack;
	bool reverb = false;
	void OnLoad()
	{
		GameObject::Register(Camera::main->GetGameObject());
		GameObject::Register(Camera::ui->GetGameObject());

		auto grid = GameObject::Instantiate("Grid");
		grid->AddComponent<Grid>();
		GameObject::Register(grid);

		auto light1 = Light::CreateDirectionalLight();
		light1->GetTransform()->Translate(10, 10, 10);
		light1->GetTransform()->LookAt(Vector3(0, 0, 0));
		light1->renderShadow = false;
		GameObject::Register(light1->GetGameObject());


		auto plane = Resource::models.Find("plane");
		plane->transform.SetScale(Vector3(20, 1, 20));
		plane->transform.Translate(0.0f, -0.01f, 0.0f);
		auto planeRenderer = plane->GetComponent<MeshRenderer>();
		planeRenderer->materials[0] = planeRenderer->materials[0]->Clone();
		planeRenderer->materials[0]->data.roughness = 0.1f;
		GameObject::Register(plane);

		Camera::main->GetTransform()->SetPosition(Vector3(0.0f, 1.0f, 2.0f));
		Camera::main->GetTransform()->LookAt(Vector3(0, 0, 0));
		Camera::main->GetGameObject()->AddComponent<KeyCam>();
		Camera::main->GetGameObject()->AddComponent<AudioListener>();

		auto sky = Camera::main->GetGameObject()->AddComponent<Skybox>();
		Camera::main->SetSkyMap(Resource::textures.Find("sky"));

		Resource::LoadFromFbx("attack01", "resources\\models", "attack01.fbx", 0.01f);
		attack = Resource::models.Find("attack01")->CloneShared();
		attack->transform.Translate(-1, 0, 0);
		attack->GetComponentInChildren<Animation>()->Play("Take 001");
		attack->GetComponentInChildren<Animation>()->speed = 0.2f;
		attack->GetComponentInChildren<MeshRenderer>()->materials[0]->data.metallic = 0.8f;
		attack->GetComponentInChildren<MeshRenderer>()->materials[0]->data.roughness = 0.5f;
		GameObject::Register(attack);

		Resource::LoadFromFbx("NightWing", "resources\\models\\nightwing", "NW.fbx", 0.1f);
		auto nightwing = Resource::models.Find("NightWing")->CloneShared();
		nightwing->transform.Translate(1, 0, 0);
		GameObject::Register(nightwing);

		Resource::LoadFromFbx("Talia", "resources\\models\\Talia", "Talia.fbx", 0.01f);
		talia = Resource::models.Find("Talia")->CloneShared();
		GameObject::Register(talia);

		auto frameCntBackground = Image::CreateImage(Vector3(-960, 540, 1), Vector2(150, 40));
		frameCntBackground->GetTransform()->SetPivot(Align(eLeft | eTop));
		frameCntBackground->SetColor(Color::Black);
		GameObject::Register(frameCntBackground->GetGameObject());

		auto text2 = Text::CreateText(Vector3(-960, 540, 1), Vector2(100, 50), L" ");
		text2->GetTransform()->SetPivot(Align(eLeft | eTop));
		text2->SetFontSize(30);
		text2->SetAlign(Align(eLeft | eTop));
		text2->GetGameObject()->AddComponent<FrameCounter>();
		text2->SetColor(Color::Green);
		text2->SetMesh();
		GameObject::Register(text2->GetGameObject());


		auto img = Image::CreateImage(Vector3(960, 540, 1), Vector2(300, 300));
		img->SetTexture(light1->GetShadowMap()->GetTexture());
		img->GetTransform()->SetPivot(Pivot(eRight | eTop));
		GameObject::Register(img->GetGameObject());
		
		auto depthMapTxt = Text::CreateText(Vector3(960, 540, 1), Vector2(300, 30), L"Shadow Map");
		depthMapTxt->SetColor(Color::Red);
		depthMapTxt->GetTransform()->SetPivot(Pivot(eRight | eTop));
		depthMapTxt->SetAlign(Align(eRight | eTop));
		GameObject::Register(depthMapTxt->GetGameObject());

		auto button = Button::CreateButton(Vector3(-960, -540, 1), Vector2(200, 50), L"");
		button->GetTransform()->SetPivot(Pivot(eLeft | eBottom));
		GameObject::Register(button->GetGameObject());
		button->SetText(L"BGM Play");
		auto as = button->GetGameObject()->AddComponent<AudioSource>();
		as->AddClip(Audio::LoadFromFile("resources\\sounds\\bgm.ogg"));

		button->onClick = bind(
			[button]() {
			auto source = button->GetGameObject()->GetComponent<AudioSource>();
			if(source->NowPlaying()) {
				source->Pause();
				button->SetText(L"BGM Play");
			}
			else {
				source->Play("bgm");
				button->SetText(L"BGM Pause");
			}
		});

		
		auto reverbButton = Button::CreateButton(Vector3(-960, -480, 1), Vector2(200, 50), L"Reverb On/Off");
		reverbButton->onClick = bind([as,this] {
			reverb = !reverb;
			as->SetReverb(reverb);
		});
		reverbButton->GetTransform()->SetPivot(Pivot(eLeft | eBottom));
		GameObject::Register(reverbButton->GetGameObject());

		auto button2 = Button::CreateButton(Vector3(960, -540, 1), Vector2(200, 50), L"Button");
		GameObject::Register(button2->GetGameObject());
		button2->GetTransform()->SetPivot(Pivot(eRight | eBottom));
		button2->SetText(L"Shadow On/Off");
		button2->GetText()->SetFontSize(26);

		button2->onClick = bind([light1]() {
			light1->renderShadow = !light1->renderShadow;
		});


		auto gridButton = Button::CreateButton(Vector3(960, -480, 1), Vector2(200, 50), L"Grid On/Off");
		gridButton->GetTransform()->SetPivot(Pivot(eRight | eBottom));
		gridButton->onClick = bind([grid]() {
			grid->SetActive(!grid->GetActive());
		});
		GameObject::Register(gridButton->GetGameObject());

		auto vsyncButton = Button::CreateButton(Vector3(960, -420, 1), Vector2(200, 50), L"VSync On/Off");
		vsyncButton->GetTransform()->SetPivot(Pivot(eRight | eBottom));
		vsyncButton->onClick = bind([]() {
			auto g = Graphic::Get();
			g->SetVsync(!g->GetVsync());
		});
		GameObject::Register(vsyncButton->GetGameObject());

		auto tip = Text::CreateText(Vector3(960, 240, 1), Vector2(300, 30), L"");
		tip->SetText(L"카메라 이동 : 우클릭&이동, 휠\n카메라 회전 : 좌클릭&이동");
		tip->SetColor(Color::Black);
		tip->GetTransform()->SetPivot(Pivot(eRight | eTop));
		tip->SetAlign(Align(eLeft | eTop));
		tip->SetFontSize(23);
		GameObject::Register(tip->GetGameObject());

		Graphic::Get()->SetVsync(true);
	}
};

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int cmdShow)
{
	auto window = GameWindow::Create(L"GameEngine", 640, 360, 60);
	window->LoadScene(std::make_shared<Scene01>());
	window->Run();
}