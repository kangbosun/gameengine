

#pragma once

class KeyCam : public Cloneable<KeyCam, Component>
{
public:
	float speed = 5;
	int prevMX = 0;
	int prevMY = 0;
	int currMX = 0;
	int currMY = 0;

	bool ldragging = false;
	bool rdragging = false;
	
	void Start()
	{
		
	}

	void Update()
	{
		// camera move
		float deltaTime = GameTime::deltaTime;
		prevMX = currMX;
		prevMY = currMY;
		currMX = (int)Input::GetMousePos().x;
		currMY = (int)Input::GetMousePos().y;
		int dx = currMX - prevMX;
		int dy = currMY - prevMY;

		if(rdragging) {
			if(dx != 0)
				GetTransform()->Rotate(Vector3(0, 1, 0), dx * 0.3f);

			if(dy != 0)
				GetTransform()->RotateSelf(Vector3(1, 0, 0),dy * 0.3f);
			if(Input::GetKeyUp(VK_RBUTTON))
				rdragging = false;
		}
		else {
			if(Input::GetKeyDown(VK_RBUTTON)) {
				prevMX = currMX = (int)Input::GetMousePos().x;
				prevMY = currMY = (int)Input::GetMousePos().y;
				rdragging = true;
			}
		}

		if(ldragging) {
			if(dx != 0)
				GetTransform()->TranslateSelf(-dx * 0.015f, 0, 0);

			if(dy != 0)
				GetTransform()->TranslateSelf(0, dy * 0.015f, 0);
			if(Input::GetKeyUp(VK_LBUTTON))
				ldragging = false;
		}
		else {
			if(Input::GetKeyDown(VK_LBUTTON)) {
				prevMX = currMX = (int)Input::GetMousePos().x;
				prevMY = currMY = (int)Input::GetMousePos().y;
				ldragging = true;
			}
		}

		if(Input::mouseWheelDelta)
			GetTransform()->TranslateSelf(0, 0, Input::mouseWheelDelta / 3 * deltaTime);
	}
};